<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestsCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->string('email');
            $table->string('emailsend');
            $table->string('phone');
            $table->string('phonesend');
            $table->string('phoneip')->nullable($value = true);
            $table->integer('build');
            $table->integer('level');
            $table->integer('room');
            $table->string('problem');
            $table->string('asproblem');
            $table->string('inventory')->nullable($value = true);
            $table->string('info')->nullable($value = true);
            $table->string('status');
            $table->string('key')->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_company');
    }
}
