<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('panel');
            $table->integer('inventory');
			$table->integer('users');
			$table->integer('department');
			$table->integer('requests');
			$table->integer('myinv');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_company');
    }
}
