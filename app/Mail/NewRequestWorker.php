<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequestWorker extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $component;
    public function __construct($component)
    {
        $this->component = $component;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this
				->from('it@support.miigaik.ru', 'Заявки МИИГАиК')
				->subject("Назначен исполнитель вашей заявки")
				->view('mail.newWorker');
        //return $this->view('mail.newWorker');
    }
}
