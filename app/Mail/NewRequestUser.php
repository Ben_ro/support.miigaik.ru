<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequestUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	public $component;
    public function __construct($component)
    {
        $this->component = $component;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
				->from('it@support.miigaik.ru', 'Заявки МИИГАиК')
				->subject("Новая заявка")
				->view('mail.newUser');
    }
}
