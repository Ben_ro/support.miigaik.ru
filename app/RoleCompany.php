<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleCompany extends Model
{
    protected $table = 'role_company';
}
