<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestsCompany extends Model
{
    protected $table = 'requests_company';
}
