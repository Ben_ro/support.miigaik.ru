<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentCompany extends Model
{
     protected $table = 'department_company';
}
