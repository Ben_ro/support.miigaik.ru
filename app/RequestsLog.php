<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestsLog extends Model
{
    protected $table = 'requests_log';
}
