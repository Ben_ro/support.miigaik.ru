<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomCompany extends Model
{
    protected $table = 'room_company';
}
