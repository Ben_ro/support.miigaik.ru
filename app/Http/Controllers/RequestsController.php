<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use App\InventoryCompany;
use App\PersonalCompany;
use App\RequestsCompany;
use App\RoomCompany;
use App\UserCompany;
use App\BuildingCompany;
use App\ProblemCompany;
use App\AsProblemCompany;
use App\StatusCompany;
use App\MassagesCompany;
use App\InventoryType;
use App\UserDepartment;
use App\RequestsLog;
use App\DepartmentCompany;
use App\User;
use App\RequestsWorker;
use App\Http\Controllers\MassageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiController;

class RequestsController extends Controller
{
	function allRe(){
		$userRequests = new UserController;
		return $userRequests->allRe();
	}
	
    public function index()
	{	
			return view('inc.requestsAdd')->with(['userRequests' => $this->allRe()]);
		
	}
	public function requestViev($id){
		$edit = 0;
		$work = 0;
		
		if(Auth::id()){
			/*
			$request = $this->get_info('requests', ['id' => $id], ['problem'], '', 1);
			$user = $this->get_info('userDepartment', ['user' => Auth::id()], ['department'], '', 1);
			$workers = $this->get_info('requestsWorker', ['request' => $id], ['worker']);
			$department = $this->get_info('department', ['parrent' => $user->department]);
			$department = $department->merge($this->get_info('department', ['id' => $user->department]));
			$accsess = 0;
			foreach($department as $dep){
				if(isset($this->get_info('problem', ['department' => $dep->id, 'id' => $request->problem], ['name'], '', 1)->name)){
					$accsess = 1;
					break;
				}
			}
			if(($accsess and Auth::user()->can('z-all-dep')) or  Auth::user()->can('z-all')){
				$edit = 1;
			}
			foreach($workers as $worker){
				if($worker->id == Auth::id()){
					$work = 1;
				}
			}*/
			
			$api = new ApiController;
			if($api->editStatus($id)){
				$work = 1;
			}
			if($api->editWorker($id)){
				$edit = 1;
			}
		}
		return view('inc.requestViev')->with(['id' => $id, 'userRequests' => $this->allRe(), 'edit' => $edit, 'work' => $work]);
	
	}
	public function getDepartmentWithProblem($problem){
		return $this->get_info('problem', ['id' => $problem], ['department'], '', 1)->department;
	}
	public function requestVievIn(Request $request){
		if($request->isMethod('post')){
			if($request->val_id){
				$val_id = $request->val_id;
				$val = $this->get_info('requests', array('key' => $val_id));
				if(!empty($val[0]->id)){
					return $this->requestViev($val[0]->id);
					//return redirect('/request/'.$request->val_id);
				}else{
					$info ='Введите верный уникальный номер!'; 
					return view('inc.requestVievIn')->with(['info' => $info, 'userRequests' => $this->allRe()]);
				}
				
			}else{
				$info ='Введите униальный номер заявки!';
				return view('inc.requestVievIn')->with(['info' => $info, 'userRequests' => $this->allRe()]);
			}
		}else{
			return view('inc.requestVievIn')->with(['userRequests' => $this->allRe()]);
		}
		
		
	}
	
	/* Все заявки для смерных */
	public function requests(){	
			return view('inc.requests')->with(['userRequests' => $this->allRe()]);
		
	}
	
	/* Вспомогательное говно */
	function get_info($table, $where = null, $select = null, $sort = ['id', 'asc'], $first = 0){
		if($sort == ''){
			$sort = ['id', 'asc'];
		}
		switch($table){
			case 'user':
				$request = new UserCompany;
				break;
			case 'users':
				$request = new User;
				break;
			case 'building':
				$request = new BuildingCompany;
				break;
			case 'room':
				$request = new RoomCompany;
				break;
			case 'requests':
				$request = new RequestsCompany;
				break;
			case 'department':
				$request = new DepartmentCompany;
				break;
			case 'requestsWorker':
				$request = new RequestsWorker;
				break;
			case 'personal':
				$request = new PersonalCompany;
				break;
			case 'inventory':
				$request = new InventoryCompany;
				break;
			case 'problem':
				$request = new ProblemCompany;
				break;
			case 'asproblem':
				$request = new AsProblemCompany;
				break;
			case 'status':
				$request = new StatusCompany;
				break;
			case 'massages':
				$request = new MassagesCompany;
				break;
			case 'info':
				$request = new MassagesCompany;
				break;
			case 'inventoryType':
				$request = new InventoryType;
				break;
			case 'userDepartment':
				$request = new UserDepartment;
				break;
			case 'requestsLog':
				$request = new RequestsLog;
				break;
		}
		if($where and $select){
			if($first){
				return $request
					->where($where)
					->select($select)
					->orderBy($sort[0], $sort[1])
					->first();
			}else{
				return $request
					->where($where)
					->select($select)
					->orderBy($sort[0], $sort[1])
					->get();
			}
		}elseif($where){
			if($first){
				return $request
					->where($where)
					->orderBy($sort[0], $sort[1])
					->first();
			}else{
				return $request
					->where($where)
					->orderBy($sort[0], $sort[1])
					->get();
			}
		}elseif($select){
			if($first){
				return $request
					->select($select)
					->orderBy($sort[0], $sort[1])
					->first();
			}else{
				return $request
					->select($select)
					->orderBy($sort[0], $sort[1])
					->get();
			}
		}else{
			return $request->all();
		}
	}

	function add_requests(){
		
	}
	/* Генерация ключа для поиска заявки */
	function RandomString(){
		$random_string = '';
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    
	    $alphnum_length = strlen($characters);
	    for($i = 0; $i < 5; $i++){
			 $random_string.= (string)$characters[rand(0, $alphnum_length - 1)];
		}
	   
	    return $random_string;
	}
	
	/* Получение заявок подразделения */
	function getRequestsDep($problems){
		$request = new RequestsCompany;
		foreach($problems as $problem){
			if($request->where(['problem' => $problem->id])->count()){
				$requests[$problem->id] = $request->where(['problem' => $problem->id])->get();
			}
		}
		if(isset($requests)){
			return $this->sortRequest($requests);
		}else{
				$requests[1] = array();
				$requests[2] = array();
				$requests[3] = array();
				return $requests;
		}
	}
	
	/* Сортировка заявок по статусу */
	function sortRequest($requests){
		$array[1] = array();
		$array[2] = array();
		$array[3] = array();
		foreach($requests as $request){
			foreach($request as $re){
				$array[$re->status][] = $re;
			}
		}
		
		return $array;
	}
}
