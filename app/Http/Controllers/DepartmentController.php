<?php

namespace App\Http\Controllers;

use App\DepartmentCompany;
use App\InventoryCompany;
use App\ProblemCompany;
use App\AsProblemCompany;
use App\BuildingCompany;
use App\RoomCompany;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RoleCompany;

use App\Http\Controllers\UserDepartmentController;

class DepartmentController extends RequestsController
{
	protected  $asprob;
	protected  $prob;
	protected  $dep;
	protected  $userDepartmnet;
	
	public function __construct(){
		$this->asprob = new AsProblemCompany;
		$this->prob = new ProblemCompany;
		$this->dep = new DepartmentCompany;
		$this->userDepartmnet = new UserDepartmentController;
	}
    public function department(Request $request){
		if($request->isMethod('post')){
			
			if(empty($request->name)){
				$info['error'] = 'Ошибка. Заполните поле "Название структурного подразделения"';
			}else{
				$this->dep->name = $request->name;
				if(!empty($request->select)){
					$this->dep->parrent = $request->select;
				}
				if($this->dep->save()){
					$info['success'] = 'Подразделение успешно добавлено!';
				}
			}
		}
		$userD = $this->get_info('userDepartment', ['user' => Auth::id()], '', '', 1);
		$dep = $this->get_info('department', ['id' => $userD->department], '', '', 1);
		
		if(Auth::user()->hasRole('admin')){
			$add = '';
		}elseif($dep->parrent){
			$add = 'disabled';
		}elseif(Auth::user()->can('dep-add') and !$dep->parrent and Auth::user()->hasRole('chief')){
			$add = 'required';
		}
		if(Auth::user()->hasRole('chief') and !$dep->parrent){
			$department = $this->dep->where(['parrent' => null, 'id' => $userD->department])->get();
		}elseif(Auth::user()->hasRole('admin')){
			$department = $this->dep->where(['parrent' => null])->get();
		}else{
			$department = [];
		}
		if(Auth::user()->hasRole('chief') and !$dep->parrent){

			$department_e = $this->dep->where(['parrent' => null, 'id' => $userD->department])->get();
			$department_e = $department_e->merge($this->dep->where(['parrent' => $userD->department])->get());
		}elseif(Auth::user()->hasRole('admin')){
			$department_e = $this->dep->get();
		}elseif(Auth::user()->hasRole('chief') and $dep->parrent){
			$department_e = $this->dep->where(['id' => $userD->department])->get();
		}else{
			$department_e = [];
		}
		if((Auth::user()->hasRole('chief') and !$dep->parrent) or Auth::user()->hasRole('admin')){
			foreach($department_e as $key => $depart){
				if($depart->parrent){
					$parrent[$depart->parrent][] = $depart;
					 unset($department_e[$key]);
				}
			}
		}else{
			$parrent = [];
		}
		if($request->isMethod('post')){	
			return view('panel.department.index')->with(['department' => $department, 'department_e' => $department_e, 'parrent' => $parrent, 'add' => $add, 'info' => $info, 'userRequests' => $this->allRe()]);
		}else{
			return view('panel.department.index')->with(['department' => $department, 'department_e' => $department_e, 'parrent' => $parrent, 'add' => $add, 'userRequests' => $this->allRe()]);
		}
	}
	function getRequestsDepartment($department){
		return $this->getRequestsDep($this->prob->where(['department' => $department])->get());
	}
	public function departmentViev(Request $request, $id){
		$department = $this->dep->where(['id' => $id])->first();
		$users = $this->userDepartmnet->getAllUserDepartment($department->id);
		if(is_array($users)){
			foreach($users as $user){
				if($user['roleId'] == 17){
					$сhief[] = $user;
				}
			}
			if(empty($сhief)){
				$сhief = '';
			}
		}else{
			$сhief = '';
			$users = '';
		}
		return view('panel.department.viev')->with(['dep' => $department , 'userRequests' => $this->allRe(),'usersDepartment' => $users, 'chief' => $сhief, 'requests' => $this->getRequestsDepartment($id)]); 
	}
	public function departmentDel(Request $request){
		$this->dep->where(['id' => $request->id])->delete();
		$this->userDepartmnet->deleteUserDepartmnet($request->id);
		$this->prob->where(['department' => $request->id])->delete();
		$this->asprob->where(['problem_id' => $this->prob->id])->delete();
		if($this->dep->where(['id' => $request->id])->count()){
			return header("HTTP/1.1 500 Internal Server Error");
		}else{
			return $this->dep->where(['id' => $request->id])->count();
		}
	}
	/*problem*/
	public function problemGet(Request $request, $department){
		$problem = $this->prob->where(['department' => $department])->get();
		echo json_encode($problem);
	}
	public function problemAdd(Request $request, $department){
		if($request->get('name') != ''){
			$this->prob->name = $request->get('name');
			$this->prob->department = $department;
			if($this->prob->save()){
				echo $this->prob->where(['id' => $this->prob->id])->first();
			}
		}
	}
	public function problemDel(Request $request, $department){
		if($request->get('id') != ''){
			if($this->prob->where(['id' => $request->get('id')])->first()->department == $department){
				return $this->prob->where(['id' => $request->get('id')])->delete();
			}
		}
	}
	/*asproblem*/
	public function asproblemGet(Request $request, $department){
		$problem = $this->asprob->where(['problem_id' => $request->get('id')])->get();
		echo json_encode($problem);
	}
	public function asproblemEdit(Request $request, $department){
		$id = $request->get('id');
		$name = $request->get('name');
		$asproblem = $this->asprob->where(['id' => $id])->first();
		$problem = $this->prob->where(['id' => $asproblem->problem_id])->first();
		if($problem->department == $department){
			print_r ($this->asprob->where(['id' => $id])->update(['name' => $name]));
		}
	}
	public function asproblemAdd(Request $request, $department){
		if($request->get('name') != ''){
			if($request->get('problem') != ''){
				$problem = $this->prob->where(['id' => $request->get('problem')])->first();
				if($problem->department == $department){
					$this->asprob->name = $request->get('name');
					$this->asprob->problem_id = $request->get('problem');
					$this->asprob->type = $request->get('checkbox');
					if($this->asprob->save()){
						echo $this->asprob->where(['id' => $this->asprob->id])->first();
					}
				}
			}
		}
	}
	public function asproblemDel(Request $request, $department){
		if($request->get('id') != ''){
		$asproblem = $this->asprob->where(['id' => $request->get('id')])->first();
			
				if($this->prob->where(['id' => $asproblem->problem_id])->first()->department == $department){
					echo $this->asprob->where(['id' => $request->get('id')])->delete();
				}
			
		}
	}
	/*info*/
	public function infoEdit(Request $request, $department){
		if($request->get('dep') == $department){
			return $this->dep->where(['id' => $department])->update(['info' => $request->get('text')]);
		}else{
			return $request->get('dep');
		}
	}
	/*address*/
	public function addressEdit(Request $request, $department){
		if($request->get('dep') == $department){
			return $this->dep->where(['id' => $department])->update(['address' => $request->get('text')]);
		}else{
			return 0;
		}
	}
	
}
