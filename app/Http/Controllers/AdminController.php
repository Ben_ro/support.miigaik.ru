<?php

namespace App\Http\Controllers;

use App\DepartmentCompany;
use App\InventoryCompany;
use App\ProblemCompany;
use App\AsProblemCompany;
use App\User;
use App\BuildingCompany;
use App\RoomCompany;
use App\UserDepartment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserDepartmentController;
use App\Http\Controllers\RoleController;

class AdminController extends RequestsController
{
	protected $ur;
	protected $ud;
	
	public function __construct(){
		$this->ur = new RoleController;
		$this->ud = new UserDepartmentController;
	}
	
    public function requestsAll(){
		//return view('inc.requestViev')->with(['id' => $id]);
		
		if(Auth::user()){
			return view('panel.requests')->with(['userRequests' => $this->allRe()]);
		}else{
			return redirect('/');
		}
		
	}
	public function typeRequest(Request $request, $id){
		
	}
	public function workerDepartment(Request $request, $id){
		
	}
	public function inventarisation(Request $request){
		return view('panel.inventory.index')->with(['userRequests' => $this->allRe()]);
	}
	public function inventarisationAdd(Request $request){
		$type = $this->get_info('inventoryType');
		if($request->isMethod('post')){
			if(!empty($request->val_build) and !empty($request->val_level) and !empty($request->val_room) and !empty($request->val_type) and !empty($request->val_inv) and !empty($request->val_info)){
				$sql = new InventoryCompany;
				if($sql->where(array('inventory_id' => $request->val_inv))->count() == 0){
					$sql->inventory_id = $request->val_inv;
					$sql->housing = $request->val_build;
					$sql->lavel = $request->val_level;
					$sql->room = $request->val_room;
					$sql->type = $request->val_type;
					$sql->info = $request->val_info;
					$sql->status = $request->val_status;
					$sql->counts = $request->val_count;
					$sql->save();
					$info = "Объект успешно добавлен в список инвентаризации";
				}else{
					$info = "Объект с таким инвентарным номером уже существует!";
				}
			}else{
				$info = "Заполните все поля $request->val_build $request->val_level $request->val_room $request->val_type $request->val_inv $request->val_info";
			}
			return view('panel.inventory.add')->with(['info' => $info, 'types' => $type, 'userRequests' => $this->allRe()]);
		}else{
			
			
			return view('panel.inventory.add')->with(['types' => $type, 'userRequests' => $this->allRe()]);
		}
		
	}
	public function inventarisationList(Request $request){
		return view('panel.inventory.list')->with(['userRequests' => $this->allRe()]);
	}
	public function inventarisationUpdateStatus(Request $request){
		$sql = new InventoryCompany;
		$sql->where('id', $request->id)->update(['status' => $request->value]);
		if($sql->where('id', $request->id)->select(['status'])->get()[0]->status == $request->value){
			return 1;
		}else{
			return 0;
		}
		//print_r($request);
		//echo json_encode('1');
		//return json_encode($request->id);
	}
	public function inventarisationListPost(){
		$inventory = $this->get_info('inventory');
		$build = $this->get_info('building');
		$rooms = $this->get_info('room');
		
		foreach($build as $bild){
			$hous[$bild->id] = $bild->name;
		}
		foreach($rooms as $room){
			$ro[$room->id] = $room->number;
		}
		foreach($inventory as $inv){
			$active = '';
			$rem = '';
			$spis = '';
			$inv->housing = $hous[$inv->housing];
			$inv->room = $ro[$inv->room];
			foreach ($this->get_info('inventoryType') as $type){
				$typeArr[$type->id] = $type->name;
			}
			$inv->type = $typeArr[$inv->type];
			switch ($inv->status) {
			    case 0:
					$active = 'selected';
			        break;
			    case 1:
					$rem = 'selected';
			        break;
			    case 2:
					$spis = 'selected';
			        break;
			}
			    
			$inv->status = "
					<select id='status-$inv->id' name='status-$inv->id' class='select-select2 col0_filter' style='width: 80%;margin: 0 10px;border: 1px solid #5ccc8b;border-radius: 3px;padding: 5px;' onchange='updateStatus(this.value, $inv->id)'>
						<option value='0' $active>Активно</option>
						<option value='1' $rem>На ремонте</option>
						<option value='2' $spis>Списан</option>
					</select>
			";
			$array[] = $inv;
		}
		print(json_encode(array('data' => $array), JSON_UNESCAPED_UNICODE));
	}
	public function inventarisationItems($id){
		$inv = $this->get_info('inventory', ['id' => $id]);
		return $inv;
	}
	public function inventarisationSearch(Request $request){
		return view('panel.inventory.search')->with(['userRequests' => $this->allRe()]);
	}
	private function get_department(){
		$sql = new DepartmentCompany;
		$department = $sql->all();
		return $department;
		
	}
	public function register(Request $request){
		$roles = $this->ur->roleList();
		if($request->isMethod('post')){
			$sql = new User;
			if($sql->where(array('email' => $request->email))->count() > 0){
				$info = 'Ошибка при добавлении пользователя! Пользователь с таким email уже существует!';
				return view('panel.users.add')->with(['info' => $info, 'request' => $request, 'department' => $this->get_department(), 'roles' => $roles, 'userRequests' => $this->allRe()]);
			}
			if($sql->where(array('name' => $request->name))->count() > 0){
				$info = 'Ошибка при добавлении пользователя! Пользователь с таким логином уже существует!';
				return view('panel.users.add')->with(['info' => $info, 'request' => $request, 'department' => $this->get_department(), 'roles' => $roles, 'userRequests' => $this->allRe()]);
			}
			$user = new User;
			$user->name = $request->name;
			$user->email = $request->email;
			$user->password = Hash::make($request->password);
			$user->fio = $request->fio;
			$user->date = $request->date;
			$user->phone = $request->phone;
			$user->type = 1;
			if($user->save()){
				if($this->ud->addUserDepartment($user->id, $request->department)){
					if($this->ur->userRoleAdd($user->id ,$request->group)){
						$info = 'Пользователь добавлен!';
					}
				}
			}
			return view('panel.users.add')->with(['info' => $info,'request' => $request, 'department' => $this->get_department(), 'roles' => $roles, 'userRequests' => $this->allRe()]);
		}else{
			return view('panel.users.add')->with(['request' => $request, 'department' => $this->get_department(), 'roles' => $roles, 'userRequests' => $this->allRe()]);
		}
		
	}
	public function user(){
		return view('panel.users.index')->with(['userRequests' => $this->allRe()]);
	}
	
	public function maps(Request $request){
		return view('panel.inventory.maps')->with(['userRequests' => $this->allRe()]);
	}
	public function map($id, Request $request){
		return view('panel.inventory.map')->with(['id' => $id, 'userRequests' => $this->allRe()]);
	}
	public function mapJson($id){
		$value  = $this->get_info('building', array('siteId' => $id));
		$array = [
				'mapwidth' => '1000',
				'mapheight' => '450',
				'levels' => []
			];
		$name = $value[0]->name;
		
		for($i = 1; $i <= $value[0]->level ; $i++){
			$rooms = $this->get_info('room', array('housing' => $value[0]->id, 'flor' => $i), array('id','number', 'mapId', 'mapX', 'mapY', 'mapTitle', 'area'));
			$types = $this->get_info('inventoryType');
			foreach ($types as $type){
				$typeArr[$type->id] = $type->name;
			}
			if($rooms->count() > 0){
				$levels = [
					'id' => 'lavel'.$i,
					"title" => "$name $i Этаж",
					"map" => "/map/svg/".$id."_".$i.".svg",
					"minimap" => "images/mall/mall-ground-mini.jpg",
					"show" => "true",
					'locations' => []
				];
				foreach($rooms as $room){
					$type_ar = [
						'mono' => 0,
						'sys' => 0,
						'moni' => 0,
					];
					$inventorys = $this->get_info('inventory', array('room' => $room->id));
					$description = '';
					if($inventorys->count() > 0){
						$description = '<hr><center><h4>Имущество в аудитории</h2></center><hr><table id="general-table" class="table table-striped table-vcenter">';
						$description .= '
						<thead>
						<tr>
							<th>Инвентарный номер</th>
							<th>Тип техники</th>
							<th>Описание</th>
							<th>Количество</th>
							<th>Статус</th>
						</tr>
						</thead>
						<tbody>
						';
						foreach($inventorys as $inv){
							if($inv->status == 0){
								switch($inv->type){
									case '5':
										$type_ar['mono']++;
									break;
									case '1':
										$type_ar['sys']++;
									break;
									case '2':
										$type_ar['moni']++;
									break;
								};
							}
							$inv->type = $typeArr[$inv->type];
							if($inv->status == 0){
								$inv->status = 'Активен';
							}elseif($inv->status == 1){
								$inv->status = 'На ремонта';
							}elseif($inv->status == 2){
								$inv->status = 'Списан';
							}
							
							
							
							$description .= "
							<tr>
								<td>$inv->inventory_id</td>
								<td>$inv->type</td>
								<td>$inv->info</td>
								<td>$inv->counts</td>
								<td>$inv->status</td>
							</tr>
							";
						}
						$description .= '</tbody></table>';
						
					}
					if($room->area == ''){
						$room->area = 0;
					}
					$area = floor($room->area / 4.5);
					if(max($type_ar) > $area){
						$enter = 'Количество ЭВМ в аудитории не соответствует СанПиН';
					}else{
						$enter = 'Количество ЭВМ в аудитории в пределах нормы';
					}
					$html = '
					<hr><center><h4>Нормы СанПиН</h2></center><hr>
					<table id="general-table" class="table table-striped table-vcenter">
					<thead>
						<tr>
							<th>Кол-во ЭВМ</th>
							<th>Площадь</th>
							<th>Макс.</th>
							<th>Кол-во / Max</th>
							<th>Результат</th>
						<tr>
					</thead>
						<tr>
							<td>'.max($type_ar).'</td>
							<td>'.$room->area.' кв.м</td>
							<td>'.$area.'</td>
							<td>'.max($type_ar).' / '.$area.'</td>
							<td>'.$enter.'</td>
						<tr>
					</table>';
					$locations = [
						"id" => "$room->mapId",
						"title" => "$room->number",
						"about" => "$room->mapTitle",
						"description" => $html.$description,
						"category" => "dep",
						"x" => "0.$room->mapX",
						"y" => "0.$room->mapY",
						"zoom" => "1"
					];
					$levels['locations'][] = $locations;
				}
				$array['levels'][] = $levels;
			}
		}
		echo json_encode($array);
	}
} 
