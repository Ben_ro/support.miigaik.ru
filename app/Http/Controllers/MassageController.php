<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MassagesCompany;
use App\Events\MassageEvent;

class MassageController extends RequestsController
{
    public function SendMassage(){
    	
		$id = request('id');
		$content['text'] = request('message');
    	$content['user'] = request('user_id'); 
    	
    	$content['created_at'] = date('Y-m-d H:i:s',time());
		if($content['user'] == 0){
			$request = $this->get_info('requests', array('key' => $id));
			$content['user_id'] = $request[0]->user; 
		}else{
			$request = $this->get_info('users', array('id' => $content['user']));
			$content['user_id'] = $request[0]->name; 
		}
		
    	
    	
    	$massage = $this->Send($id , $content['user'], $content['text']);
    	//print_r($massage);
    	//event(new MassageEvent($content));
    	//event(new \App\Events\MassageEvent($content));
    	//broadcast(new \App\Events\MassageEvent($content));
    	event(new MassageEvent($id, $content));
    	return($content);
		//echo '1';
	}
	public function GetMassages($id){
		$massages = $this->get_info('massages', array('request_id' => $id), array('id', 'text', 'user_id', 'request_id', 'created_at'), array('created_at', 'asc'));
		foreach($massages as $massage){
			if($massage->user_id != 0){
				$request = $this->get_info('users', array('id' => $massage->user_id));
				$massage->user_id = $request[0]->name;
			}else{
				$request = $this->get_info('requests', array('key' => $massage->request_id));
				$massage->user_id = $request[0]->user; 
			}
			
		}
    	//$massage = $this->Send($request->id, $request->user_id, $request->text);
    	
    	//event(new MassagesCompany($massage));
		//echo '1';
		return json_encode($massages);
	}
	function Send($request_id, $user_id, $massage){
		$requests = new MassagesCompany;
    	$requests->request_id = $request_id;
    	$requests->user_id = $user_id;
    	$requests->text = $massage;
    	$requests->save();
	}
	public function AllMassage($request){
		$massages = $this->get_info('massages', array('request_id' => $request), '', array('created_at', 'desc'));
		foreach($massages as $massage){
			$dt_elements = explode(' ', $massage->created_at);
			$date_elements = explode('-',$dt_elements[0]);
			$time_elements =  explode(':',$dt_elements[1]);
			$massage->date = mktime($time_elements[0], $time_elements[1],$time_elements[2], $date_elements[1],$date_elements[2], $date_elements[0]);
			if($massage->user_id != 0){
				$massage->user = $this->get_info('user', array('id' => $massage->user_id))[0]->name;
			}else{
				$massage->user = $this->get_info('requests', array('key' => $request))[0]->user;
			}

		}
		return $massages;
	}
	
}
