<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserDepartment;
use App\User;
use App\Http\Controllers\RoleController;

class UserDepartmentController extends Controller
{
	protected $userDepartment;
	protected $user;
	
	public function __construct(){
		$this->userDepartment = new UserDepartment;
		$this->user = new User;
		$this->getRole = new RoleController;
	}
	
    public function getAllUserDepartment($departmnet){
		$usersDepartment = $this->userDepartment->where(['department' => $departmnet])->get();
		if(count($usersDepartment)){
			foreach($usersDepartment as $ud){
				$users[] = $this->user->where(['id' => $ud->user])->first();
				
			}
			
			$users = $this->getRole->getRoleUser($users);
			return $users;
		}else{
			return '';
		}
	}
	
	public function getUserDepartment($user){
		return $this->userDepartment->where(['user' => $user])->first();
	}
	public function addUserDepartment($user, $department){
		$this->userDepartment->user = $user;
		$this->userDepartment->department = $department;
		return $this->userDepartment->save();
	}
	public function deleteUserDepartmnet($department, $user = ''){
			if($user != ''){
				return $this->userDepartment->where(['department' => $department, 'user' => $user])->delete();
			}else{
				return $this->userDepartment->where(['department' => $department])->delete();
			}
	}
}
