<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\PermissionRole;
use App\RoleUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Seeder;
use App\Http\Controllers\UserController;

class RoleController extends Controller
{
	protected  $userRequests;
	protected  $roleUser;
	protected  $roles;
	protected  $permissionRole;
	protected  $permission;
	
	public function __construct(){
		$this->userRequests = new UserController;
		$this->roleUser = new RoleUser;
		$this->roles = new Role;
		$this->permissionRole = new PermissionRole;
		$this->permission = new Permission;
	}
	function getRoleUser($array){
		foreach($array as $user){
			$first = $this->roleUser->where(['user_id' => $user->id])->first();
			$user['roleId'] = $first['role_id'];
			$user['roleName'] = $this->roles->where(['id' => $first['role_id']])->first()['display_name'];
			//print_r($user);
			//die;
		}
		return $array;
	}
	public function role(Request $request){
		$arr = [];
		$permission = [];
		foreach($this->permissionRoleList() as $perm){
			$arr[$perm['role_id']][] = $perm['permission_id'];
		}
		
		foreach($this->roleList() as $role){
			foreach($this->permissionList() as $perm){
				if(isset($arr[$role['id']])){
					if(array_search($perm->id ,$arr[$role['id']]) !== false){
						$permissionListRole[$role['id']][$perm['id']] = array(1,$perm['id']);
					}else{
						$permissionListRole[$role['id']][$perm['id']] = array(0,$perm['id']);
					}
				}else{
					$permissionListRole[$role['id']][$perm['id']] = array(0,$perm['id']);
				}
			}
		}
		return view('panel.users.role')->with(['request' => $request, 'roles' => $this->roleList(), 'permissions' => $this->permissionList(), 'permissionRoleList' => $permissionListRole, 'userRequests' => $this->userRequests->allRe()]);
	}
	public function infoAdd(Request $request){
		if($request->isMethod('post')){
			if(!empty($request->type)){
				switch ($request->type){
					case 'roleAdd':
						$this->roleCreate($request);
						break;
					case 'premissionAdd':
						$this->permissonCreate($request);
						break;
					case 'roleRemove':
						$this->roleRemove($request);
						break;
					case 'permissionRemove':
						$this->permissionRemove($request);
						break;
				}
			}elseif($request->user){
				$role = Role::findOrFail($request->role);
				$user = User::where( 'id' , '=' , '2' )->first();
				$user->attachRole($role);
			}
		}
		//return redirect()->action('RoleController@role')->with( [ 'id' => $id ] );;
		return redirect()->action('RoleController@role');
		//return Redirect::route('clients.show, $id')->with( ['data' => $data] );
		//return Redirect::route('clients.show, $id');
	}
	public function permissionAdd(Request $request){
		foreach($request['rol'] as $roleId => $roled){
			$role = Role::findOrFail($roleId);
			PermissionRole::where('role_id', $roleId)->delete();
			foreach($roled as $permissionId => $permission){
				$permissions = Permission::findOrFail($permissionId);	
				$role->attachPermission($permissions);
			}
		}
		return redirect()->action('RoleController@role');
	}
    function roleCreate($request){
		$this->roles->name         = $request->name;
		$this->roles->display_name = $request->display_name;
		$this->roles->description  = $request->description;
		$this->roles->save();
	}
	function permissonCreate($request){
		$this->permission->name         = $request->name;
		$this->permission->display_name = $request->display_name;
		$this->permission->description  = $request->description;
		$this->permission->save();
	}
	function roleRemove($request){
		$role = Role::where('name', $request->name);
		$role->delete();
	}
	function permissionRemove($request){
		$permission = Permission::where('name', $request->name);
		$permission->delete();
	}
	function roleList(){
		return $this->roles->all();
	}
	function permissionList(){
		return $this->permission->all();
	}
	function permissionRoleList($id = ''){
		if($id == ''){
			return $this->permissionRole->all();
		}else{
			return $this->permissionRole->where(['role_id' => $id])->get();
		}
	}
	public function userRoleAdd($user, $role){
		$this->roleUser->user_id = $user;
		$this->roleUser->role_id = $role;
		return $this->roleUser->save();
	}
	public function userRoleDel($user){
		$this->roleUser->where(['user_id' => $user])->delete();
	}
}
