<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\RequestsCompany;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Permission;
use App\PermissionRole;
use App\RequestsWorker;
use App\RoleUser;
use App\RequestsLog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SmsController;
use App\Mail\NewRequestUser;
use App\Mail\UpdateRequestUser;
use App\Mail\NewRequestWorker;
use Illuminate\Support\Facades\Mail;

class ApiController extends RequestsController
{
	public $sms;
	function __construct(){
		$this->smsc = new SmsController;
	}
    public function apiRequestsAdd(Request $request){
		$key = $this->RandomString();
		$answer = new \stdClass();
		$requests = new RequestsCompany;
		$requests->user = $request->val_username;
		$requests->email = $request->val_email;
		$requests->emailsend = $request->val_emailsend;
		$requests->phone = $request->val_phone;
		$requests->phonesend = $request->val_phonesend;
		if(!empty($request->val_phoneip)){
			$requests->phoneip = $request->val_phoneip;
		}
		$requests->build = $request->val_build;
		$requests->level = $request->val_level;
		$requests->room = $request->val_room;
		$requests->problem = $request->val_problem;
		if(json_encode($request->val_asproblem)){
			$requests->asproblem = json_encode($request->val_asproblem);
		}
		if(!empty($request->val_inventory)){
			$requests->inventory = $request->val_inventory;
		}
		if(!empty($request->val_info)){
			$requests->info = $request->val_info;
		}
		$requests->status = '1';
		$requests->key = $key;
		$requests->save();
		//foreach($request as $value){
		//	$array[] = $value;
		//}
		$answer->key = $key;
		$answer->id = $requests->id;
		if($requests->id){
			$answer->value = 'sucсess';
			$mail = new \stdClass();
			$mail->key = $key;
			$mail->number = $requests->id;
			$mail->problem = $this->get_info('problem', ['id' => $request->val_problem], ['name'], '', 1)->name;
			if(!empty($request->val_info)){
				$mail->comment = $request->val_info;
			}else{
				$mail->comment = '';
			}
			Mail::to($request->val_email)->send(new NewRequestUser($mail));
			//$this->smsc->sms(1, $mail, $requests->phone);
			
		}else{
			$answer->value = 'error';
		}
		return json_encode($answer);
		
	}
	public function apiUsers(){
		
		$obj = new \stdClass();
		
		foreach($this->get_info('user', '', 'name') as $user){
			$array[] = $user->name;
		};
		$obj->user =  $array;
		$array = array();
		foreach($this->get_info('building', '', array('id', 'name', 'level')) as $build){
			$array[] = $build;
		}
		$obj->building =  $array;
		$array = array();
		foreach($this->get_info('room', '', array('id', 'number', 'flor', 'housing')) as $room){
			$array[] = $room;
		}
		$obj->room =  $array;
		$array = array();
		foreach($this->get_info('problem', '', array('id', 'name')) as $problems){
			$array[] = $problems;
		}
		$obj->problems =  $array;
		$array = array();
		foreach($this->get_info('asproblem', '', array('id', 'name', 'ename', 'type', 'problem_id')) as $asproblem){
			$array[] = $asproblem;
		}
		$obj->asproblem =  $array;
		$array = array();
		return json_encode($obj, JSON_UNESCAPED_UNICODE);
		
	}
	public function apiRequests(){
		foreach($this->get_info('requests', '', array('id', 'problem', 'status', 'created_at')) as $request){
			$request->problem = $this->get_info('problem', array('id'=>$request->problem), array('name'))[0]->name;
			$request->idd = '<center>'.$request->id.'</center>';
			if($status = $this->get_info('status', array('id' => $request->status), array('name', 'ename'))){
				$request->status = '<center><span class="label label-'.$status[0]->ename.'">'.$status[0]->name.'</span></center>';
				//$request->status = $status[0]->name;
			}
			
			$array['data'][] = $request;
		}
		
		return json_encode($array, JSON_UNESCAPED_UNICODE);
	}
	function prDep($id){
		
	}
	function editStatus($id){
		if(Auth::check()){
			$worker = $this->get_info('requestsWorker', array('request' => $id, 'worker' => Auth::id()));
			
			if(count($worker) or $this->editWorker($id)){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	function editWorker($id){
		if(Auth::check()){
			if(Auth::user()->can('request-edit-all')){
				return 1;
			}
			$user = $this->get_info('userDepartment', ['user' => Auth::id()], '', '', 1);
			$request = $this->get_info('requests', ['id' => $id], ['problem'], '', 1);
			$department = $this->get_info('department', ['id' => $user->department]);
			$department = $department->merge($this->get_info('department', ['parrent' => $user->department]));
			$accsess = 0;
			foreach($department as $dep){
				if(isset($this->get_info('problem', ['department' => $dep->id, 'id' => $request->problem], '', '', 1)->name)){
					$accsess = 1;
					break;
				}
			}
			if($accsess and Auth::user()->can('request-edit-dep')){
				return 1;
			}else{
				return 0;
			}			
		}else{
			return 0;
		}
	}
	public function requestStatusUpdate(Request $request){
		if($this->editStatus($request->request_id)){
			$req = new RequestsCompany;
			$output = $req->where(['id' => $request->request_id])->update(['status' => $request->status_id]);
			//print_r($output);
			
			switch($request->status_id){
				case 1:
					$status = 'new';
					$text = 'Новая';
					break;
				case 2:
					$status = 'work';
					$text = 'В работе';
					break;
				case 3:
					$status = 'close';
					$text = 'Закрыта';
					break;
			}
			$log = new RequestsLog;
			$log->request = $request->request_id;
			$log->type = $status;
			$log->text = 'Изменен статус заявки: '.$text;
			$log->user = Auth::id();
			$log->save();
			if($output == 1){
				$ifMail = $req->where(['id' => $request->request_id])->first();
				if($ifMail->emailsend == 'on'){
					$mail = new \stdClass;
					$mail->status = $text;
					$mail->id = $request->request_id;
					$mail->key = $ifMail->key;
					Mail::to($ifMail->email)->send(new UpdateRequestUser($mail));
				}
			}
		}
	}
	public function requestWorkerAdd(Request $request){
		if($this->editWorker($request->request_id)){
			$reqw = new RequestsWorker;
			if($request->worker_id){
				if(!count($reqw->where(['request' => $request->request_id, 'worker' => $request->worker_id])->get())){
					$reqw->request = $request->request_id;
					$reqw->worker = $request->worker_id;
					$reqw->type = $request->worker_type;
					$reqw->save();
					if($reqw->id){
						//return 1;
						$user = $this->get_info('users', ['id' => $request->worker_id], ['id', 'fio', 'phone', 'email'], '', 1);
						$req = $this->get_info('requests', ['id' => $request->request_id], ['key', 'email', 'emailsend'], '', 1);
						
						
						$log = new RequestsLog;
						$log->request = $request->request_id;
						$log->type = 'add';
						if($request->worker_type == 1){
							$log->text = 'Добавлен исполнитель заявки '.$user->fio;
						}elseif($request->worker_type == 2){
							$log->text = 'Добавлен соисполнитель заявки '.$user->fio;
						}
						$log->user = Auth::id();
						$log->save();
						if($req->emailsend){
							$mail = new \stdClass;
							$mail->id = $request->request_id;
							$mail->key = $req->key;
							$mail->user = $user;
							Mail::to($req->email)->send(new NewRequestWorker($mail));
						}
						return json_encode($user, JSON_UNESCAPED_UNICODE);
					}else{
						return 0;
					}
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	public function requestWorkerDel(Request $request){
		if($this->editWorker($request->request_id)){
			$req = new RequestsWorker;
			if(count($req->where(['request' => $request->request_id, 'worker' => $request->worker_id])->get())){
				$req->where(['request' => $request->request_id, 'worker' => $request->worker_id, 'type' => $request->worker_type])->delete();
				$user = $this->get_info('users', ['id' => $request->worker_id], ['fio']);
				$log = new RequestsLog;
				$log->request = $request->request_id;
				$log->type = 'del';
				if($request->worker_type == 1){
					$log->text = 'Удалён исполнитель заявки '.$user[0]->fio;
				}elseif($request->worker_type == 2){
					$log->text = 'Удалён соисполнитель заявки '.$user[0]->fio;
				}
				$log->user = Auth::id();
				$log->save();
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	public function apiRequestViev(Request $request){
		return($this->RequestViev($request->val_id));
	}
	public function RequestViev($code){
		if(empty($code)){
			return abort(404);
		}
		$requests = $this->get_info('requests', ['id' => $code], ['id', 'user', 'problem', 'inventory', 'status', 'info', 'email', 'phone', 'phoneip', 'build', 'level', 'room', 'asproblem', 'status'], '', 1);
		$requests->build = $this->get_info('building', ['id' => $requests->build], '', '', 1)->name;
		$requests->room = $this->get_info('room', ['id' => $requests->room], '', '', 1)->number;
		$problem = $this->get_info('problem', ['id' => $requests->problem], '', '', 1);
		$requests->problem = $problem->name;
		$requests->eworker = 0;
		$requests->estatus = 0;
		$workers = $this->get_info('requestsWorker', ['request' => $requests->id, 'type' => 1]);
		$soworkers = $this->get_info('requestsWorker', ['request' => $requests->id, 'type' => 2]);
		$user_sql = new User;
		$user = array();
		foreach($workers as $work){
			$user[] = $user_sql->where(['id' => $work->worker])->select(['id', 'fio', 'phone', 'email'])->firstOrFail();
		}
		$requests->worker = $user;
		$user = array();
		foreach($soworkers as $work){
			$user[] = $user_sql->where(['id' => $work->worker])->select(['id', 'fio', 'phone', 'email'])->firstOrFail();
		}
		$requests->soworker = $user;
		$requests->wdepartment = array();
		
		if($this->editWorker($requests->id)){
			$requests->eworker = 1;
			$dep_work = $this->get_info('userDepartment', ['department' => $problem->department]);
			if(count($dep_work)){
				foreach($dep_work as $worker){
					
					$users[] = $user_sql->where(['id' => $worker->user])->select(['id', 'fio', 'phone', 'email'])->firstOrFail();
				}
				$requests->wdepartment = $users;
			}
		}else{
			$requests->eworker = 0;
		}
		if($this->editStatus($requests->id)){
			$requests->estatus = 1;
		}else{
			$requests->estatus = 0;
		}
		$status = $this->get_info('status', ['id' => $requests->status], '', '', 1);
		$requests->status = '<span class="h2 text-'.$status->ename.' animation-expandOpen">'.$status->name.'</span>';
		$asproblem = json_decode($requests->asproblem);
		for($i = 0; $i < count($asproblem); $i++){
			if($i > 0){
				$requests->asproblem .= ', '.$this->get_info('asproblem', ['id' => $asproblem[$i]], '', '', 1)->name;
			}else{
				$requests->asproblem = $this->get_info('asproblem', ['id' => $asproblem[$i]], '', '', 1)->name;
			}
			
		}
		return json_encode($requests, JSON_UNESCAPED_UNICODE);
	}
	public function adminRequestsAll(){
		if(Auth::check()){
			
			$status = $this->get_info('status');
			if(Auth::user()->can('z-all')){
				$problem = $this->get_info('problem');
				$requests = $this->get_info('requests');
				foreach($requests as $req){
					$id = $req->problem - 1;
					$id_status = $req->status - 1;
					$req->problem = "<a href='https://support.miigaik.ru/panel/user/requests/viev/{$req->id}' target='_blank'>{$problem[$id]->name}</a>";
					$req->idd = '<center>'.$req->id.'</center>';
					$req->status = '<center><span class="label label-'.$status[$id_status]->ename.'">'.$status[$id_status]->name.'</span></center>';
					$array['data'][] = $req;
				}
				return json_encode($array);
			}elseif(Auth::user()->can('z-all-dep')){
				$user = $this->get_info('userDepartment', ['user' => Auth::id()], ['department'], '', 1);
				$department = $this->get_info('department', ['parrent' => $user->department]);
				//print_r($department);
				//die;
				$problems = $this->get_info('problem', ['department' =>	$user->department]);
				foreach($problems as $problem){
					$requests = $this->get_info('requests', ['problem' => $problem->id]);
					foreach($requests as $req){
						$id_status = $req->status - 1;
						$req->problem = "<a href='https://support.miigaik.ru/panel/user/requests/viev/{$req->id}' target='_blank'>{$problem->name}</a>";
						$req->idd = '<center>'.$req->id.'</center>';
						$req->status = '<center><span class="label label-'.$status[$id_status]->ename.'">'.$status[$id_status]->name.'</span></center>';
						$array['data'][] = $req;
					}
				}
				if(count($department)){
					foreach($department as $dep){
						$problems = $this->get_info('problem', ['department' =>	$dep->id]);
						foreach($problems as $problem){
							$requests = $this->get_info('requests', ['problem' => $problem->id]);
							foreach($requests as $req){
								$id_status = $req->status - 1;
								$req->problem = "<a href='https://support.miigaik.ru/panel/user/requests/viev/{$req->id}' target='_blank'>{$problem->name}</a>";
								$req->idd = '<center>'.$req->id.'</center>';
								$req->status = '<center><span class="label label-'.$status[$id_status]->ename.'">'.$status[$id_status]->name.'</span></center>';
								$array['data'][] = $req;
							}
						}
						
					}
				}
					
				
				return json_encode($array);
			}else{
				return redirect()->route('home');
			}
				
		}else{
			return redirect()->route('home');
		}
	}
	public function requestsLog(Request $request){
		$logs = $this->get_info('requestsLog', ['request' => $request->request_id], '', ['created_at', 'DESC']);
		foreach($logs as $log){
			if($log->user){
				$log->user = $this->get_info('users', ['id' => $log->user], ['fio'])[0]->fio;
			}else{
				$log->user = 'Система';
			}
			$time = explode(' ', $log->created_at);
			$log->times = $time[1].'<br><strong>'.$time[0].'</strong>';
			switch ($log->type){
				case 'add':
					$log->type = '<i class="gi gi-user_add"></i>';
					break;
				case 'del':
					$log->type = '<i class="gi gi-user_remove"></i>';
					break;
				case 'new':
					$log->type = '<i class="gi gi-chevron-right"></i>';
					break;
				case 'work':
					$log->type = '<i class="gi gi-chevron-right"></i>';
					break;
				default:
					$log->type = '<i class="gi gi-chevron-right"></i>';
					break;
			}
		}
		return json_encode($logs, JSON_UNESCAPED_UNICODE);
	}

}
