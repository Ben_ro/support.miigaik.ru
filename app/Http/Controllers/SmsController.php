<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SmsController extends Controller
{
    function __construct(){
		
	}
	function sendSms($phone, $text){
		$phone = preg_replace('/[^0-9]/', '', $phone);
		$url = 'http://10.1.100.210/sendsms?username=smsuser&password=smsuserpassword&phonenumber=+'.$phone.'&message='.$text.'&[port=1.3&][report=1&][timeout=30]';
		$html = file_get_contents($url);
	}
	public function sms($type, $text, $phone){
		switch($type){
			case 1:
				$text = 'Номер заявки: '.$text->number.'\nКод отслеживания: '.$text->key.'\nСсылка на сайт: https://support.miigaik.ru/';
				break;
			case 2:
				$text = urlencode('456 456');
				break;
		}
		$text = urlencode($text);
		$text = str_replace('%5Cn', '%0A', $text);
		return $this->sendSms($phone, $text);
	}
}
