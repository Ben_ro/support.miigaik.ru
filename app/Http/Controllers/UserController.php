<?php

namespace App\Http\Controllers;

use App\RequestsWorker;
use App\RequestsCompany;
use App\ProblemCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\RequestsController;

class UserController extends Controller
{
	public function requests(){
		return view('panel.user.requests')->with(['userRequests' => $this->allRe()]);
	}
	public function getRequests($id){
		switch ($id){
			case '1':
				$text = '<center><span class="label label-info">Новая</span></center>';
				break;
			case '2':
				$text = '<center><span class="label label-warning">В работе</span></center>';
				break;
			case '3':
				$text = '<center><span class="label label-success">Закрыта</span></center>';
				break;
		}
		foreach($this->getWorkerRequests() as $req){
			$info = $this->getRequestStatus($req->request);
			if($info->status == $id){
				$info->problemHtml = '<a href="/panel/user/requests/viev/'.$req->request.'">'.$this->getProblem($info->problem)->name.'</a>';
				$array['data'][] = $info;
			}
		}
		if(!empty($array)){
			return json_encode($array);
		}else{
			return ['data' => []];
		}
		
		//print_r($info);
	}
	public function chengeStatus(Request $request){
		
	}
	public function vievRequests($id){
		$api = new ApiController;
		return $api->RequestViev($id);
	}
	function getWorkerRequests(){
		$sql = new RequestsWorker;
		return $sql->where(['worker' => Auth::id()])->get();
	}
	function getRequestStatus($id){
		$sql = new RequestsCompany;
		return $sql->where(['id' => $id])->first();
	}
	function getProblem($id){
		$sql = new ProblemCompany;
		return $sql->where(['id' => $id])->first();
	}
	function getRequestStatusGood($id){
		if($this->getRequestStatus($id)->status < 2){
			return 1;
		}else{
			return 0;
		}
	}
	function allRe(){
		$i = 0;
		foreach($this->getWorkerRequests() as $request){
			if($this->getRequestStatusGood($request->request)){
				$i++;
			}
		}
		return $i;
	}
}
