<?php

namespace App\Http\Controllers;

use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Test extends Controller
{
    public function mail_test(){
		$order = '123';
		Mail::to('lit@miigaik.ru')->send(new OrderShipped($order));
	}
}
