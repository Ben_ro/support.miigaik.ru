<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProblemCompany extends Model
{
    protected $table = 'problem_company';
}
