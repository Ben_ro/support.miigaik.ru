<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsProblemCompany extends Model
{
    protected $table = 'asproblem_company';
}
