<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCompany extends Model
{
    protected $table = 'inventory_company';
}
