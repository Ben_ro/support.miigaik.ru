<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'RequestsController@index');
Route::get('/requests', 'RequestsController@requests');
Route::get('/request/{id}', 'RequestsController@requestViev');
Route::get('/request', 'RequestsController@requestVievIn');
Route::post('/request', 'RequestsController@requestVievIn');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*Route::get('test-broadcast', function(){
    event(new \App\Events\MassageEvent('Hallo world!'));
});*/

/* PANEL */
	/* Requests */
		Route::group(['middleware' => ['permission:z-all|z-all-dep']], function() {
			Route::get('/panel/requests', 'AdminController@requestsAll')->middleware('auth');
		});
	/* Department */
	
		Route::group(['middleware' => ['permission:dep-add']], function() {
			Route::get('/panel/department', 'DepartmentController@department')->middleware('auth');
			Route::post('/panel/department', 'DepartmentController@department')->middleware('auth');
			Route::post('/panel/department/del', 'DepartmentController@departmentDel')->middleware('auth');
		});
		Route::group(['middleware' => ['permission:dep-viev']], function() {
			Route::get('/panel/department/{id}', 'DepartmentController@departmentViev')->middleware('auth');
		});
		Route::get('/panel/department/{department}/type/get', 'DepartmentController@problemGet')->middleware('auth');
		Route::get('/panel/department/{department}/type/add', 'DepartmentController@problemAdd')->middleware('auth');
		Route::get('/panel/department/{department}/type/del', 'DepartmentController@problemDel')->middleware('auth');
		
		Route::get('/panel/department/{department}/astype/get', 'DepartmentController@asproblemGet')->middleware('auth');
		Route::get('/panel/department/{department}/astype/edit', 'DepartmentController@asproblemEdit')->middleware('auth');
		Route::get('/panel/department/{department}/astype/add', 'DepartmentController@asproblemAdd')->middleware('auth');
		Route::get('/panel/department/{department}/astype/del', 'DepartmentController@asproblemDel')->middleware('auth');
		
		Route::get('/panel/department/{department}/info/edit', 'DepartmentController@infoEdit')->middleware('auth');
		Route::get('/panel/department/{department}/address/edit', 'DepartmentController@addressEdit')->middleware('auth');
		
	/* Inventory */
	Route::group(['middleware' => ['permission:inv-add|inv-maps|inv-spisok']], function() {
		Route::get('/panel/inventory', 'AdminController@inventarisation')->middleware('auth');
		Route::group(['middleware' => ['permission:inv-add']], function() {
			Route::get('/panel/inventory/add', 'AdminController@inventarisationAdd')->middleware('auth');
			Route::post('/panel/inventory/add', 'AdminController@inventarisationAdd')->middleware('auth');
		});
		Route::group(['middleware' => ['permission:inv-maps']], function() {
			Route::get('/panel/inventory/maps', 'AdminController@maps')->middleware('auth');
			Route::get('/panel/inventory/maps/{id}', 'AdminController@map')->middleware('auth');
			Route::get('/panel/inventory/mapjson/{id}', 'AdminController@mapJson')->middleware('auth');
		});
		Route::group(['middleware' => ['permission:inv-spisok']], function() {
			Route::get('/panel/inventory/list', 'AdminController@inventarisationList')->middleware('auth');
			Route::post('/panel/inventory/updateStatus', 'AdminController@inventarisationUpdateStatus')->middleware('auth');
			Route::get('/panel/inventory/listGet', 'AdminController@inventarisationListPost')->middleware('auth');
		});
		//Route::get('/panel/inventory/search', 'AdminController@inventarisationSearch')->middleware('auth');
		//Route::get('/panel/inventory/item/{id}', 'AdminController@inventarisationItems')->middleware('auth');
	});
	/*Users setings*/
	
		Route::group(['middleware' => ['permission:users']], function() {
			Route::get('/panel/users', 'AdminController@user')->middleware('auth');
			Route::group(['middleware' => ['permission:users-role']], function() {
				Route::get('/panel/users/role', 'RoleController@role')->middleware('auth');
				Route::post('/panel/users/role/infoAdd', 'RoleController@infoAdd')->middleware('auth');
				Route::post('/panel/users/role/permission', 'RoleController@permissionAdd')->middleware('auth');
			});
			Route::group(['middleware' => ['permission:user-add']], function() {
				Route::get('/panel/users/add', 'AdminController@register')->middleware('auth');
				Route::post('/panel/users/add', 'AdminController@register')->middleware('auth');
			});
		});
	/*User info*/
		Route::get('/panel/user/requests', 'UserController@requests')->middleware('auth');
		Route::get('/panel/user/requests/viev/{id}', 'RequestsController@requestViev')->middleware('auth');
		Route::get('/panel/user/requests/{id}', 'UserController@getRequests')->middleware('auth');
	
/* API */
	Route::get('/api/requests/all', 'ApiController@apiRequests');
	Route::post('/api/request', 'ApiController@apiRequestViev');
	Route::post('/api/request/status', 'ApiController@requestStatusUpdate')->middleware('auth');
	Route::post('/api/request/worker/add', 'ApiController@requestWorkerAdd')->middleware('auth');
	Route::post('/api/request/worker/del', 'ApiController@requestWorkerDel')->middleware('auth');
	Route::post('/api/request/log', 'ApiController@requestsLog');
	Route::get('/api/users', 'ApiController@apiUsers');
	Route::post('/api/requests/add', 'ApiController@apiRequestsAdd');
	Route::get('/api/admin/requests', 'ApiController@adminRequestsAll');

/* CHAT */
	Route::post('/chat/send', 'MassageController@SendMassage');
	Route::get('/chat/get/{id}', 'MassageController@GetMassages');