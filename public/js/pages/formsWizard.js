/*
 *  Document   : formsWizard.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Wizard page
 */

function json(){
	var request = new XMLHttpRequest();
	request.open('GET', '/api/users', false);
	request.send( null );
    return JSON.parse(request.responseText);
}

//document.documentElement.style.display = 'block';
/*$("#next4").click(function() {
	if()
  $('#modal-terms').slideToggle();
  $("#modal-terms").addClass('in').html();
});*/
var arr = json();
if(arr){
	var building_select = '<option></option>';
	$('.var_user').typeahead({ source: arr.user });
    arr.building.forEach(function(element) {
    	building_select +=  '<option value="'+element.id+'">'+element.name+'</option>';
		//console.log(element.id);
	}); 
	document.getElementById("val_build").innerHTML=building_select;
	var problems_select =  '<option></option>';
	arr.problems.forEach(function(element) {
    	problems_select +=  '<option value="'+element.id+'">'+element.name+'</option>';
		//console.log(element.id);
	}); 
	document.getElementById("val_problem").innerHTML=problems_select;
}
function problem_get(val){  
	var problem_label = '';
	var ploblem_input = '';  
	var i = 0;
	if(arr.asproblem){
		arr.asproblem.forEach(function(element) {
			if(element.problem_id == val){
				if(element.type == 'label'){
					problem_label += '<label class="checkbox-inline" for="example-inline-checkbox'+element.id+'">';
					problem_label += '<input type="checkbox" id="example-inline-checkbox'+element.id+'" name="problem-checkbox" value="'+element.id+'">'+element.name;
					problem_label += '</label></br>';
				}else if(element.type == 'input'){
					ploblem_input += '<div class="form-group">';
					ploblem_input += '<label class="col-md-4 control-label" for="example-clickable-firstname">'+element.name+'</label>';
					ploblem_input += '<div class="col-md-5">';
					ploblem_input += '<input type="text" id="val_'+element.ename+'" name="val_'+element.ename+'" class="form-control var_user" autocomplete="off" placeholder="" required>';
					ploblem_input += '</div></div>';
				}
			}	
		});
	}
	if(problem_label){
		document.getElementById('form-label').style.display = "block";	
		document.getElementById('label').innerHTML=problem_label;
	}else{
		document.getElementById('form-label').style.display = "none";	
	}
	
	document.getElementById('form-input').innerHTML=ploblem_input;
	
	
	console.log(val);
}
$('#val_terms').on('click', function(){
	if($("#val_terms").is(':checked')){
		$('#next4').attr("onclick","postSend()")
	}else{
		$('#next4').attr("onclick","")
	}
	
    //document.getElementById('next4').attr("onclick","postSend();");
});
function postSend(){
	
// make an ajax request to a PHP file
// on our site that will update the database
// pass in our lat/lng as parameters
	//var rules = document.getElementById('val_terms').getAttribute("aria-invalid");
	//document.getElementById("contact-email").getAttribute("aria-invalid")
	
	if($("#val_terms").is(':checked')){
		var array = new Array();
		var select = document.getElementsByName('problem-checkbox');
		var i=0;
		select.forEach(function(element){
			if(element.checked){
				array[i] = element.value;
				i++;
			}
		});
		if($('#val_emailsend:checked').val()){
			var emailsend = 'on';
		}else{
			var emailsend = 'off';
		}
		if($('#val_phonesend:checked').val()){
			var phonesend = 'on';
		}else{
			var phonesend = 'off';
		}
		if($('#val_inventory:checked')){
			var inventory = $('#val_inventory:checked').val();
		}else{
			var inventory = '';
		}
		$.post('/api/requests/add', {
			_token: $('meta[name=csrf-token]').attr('content'),
			val_username: document.getElementById('val_username').value,
			val_email: document.getElementById('val_email').value,
			val_phone: document.getElementById('val_phone').value,
			val_phoneip: document.getElementById('val_phoneip').value,
			val_build: document.getElementById('val_build').value,
			val_level: document.getElementById('val_level').value,
			val_room: document.getElementById('val_room').value,
			val_problem: document.getElementById('val_problem').value,
			val_info: document.getElementById('val_info').value,
			val_emailsend: emailsend,
			val_phonesend: phonesend,
			val_inventory: inventory,
			val_asproblem: array
			//newLng: newLng
		})
		.done(function(data) {
			var array = JSON.parse(data);
			var i = 0;
			var info = '';
			console.log(array);
			if(array.value == 'sucсess'){
				info += '<div class="alert alert-success animation-fadeInQuick"><center><h3>Заявка <strong>№'+array.id+'</strong> успешно отправлена!</h3><h3>Уникальный номер для получения доступа к заявке: <strong>'+array.key+'</strong> </h3></center><h4><i class="fa fa-check"></i> Для просмотра статуса заявки вы можете воспользоваться этой ссылой и ввести уникальный код: <a href="/request"> ссылка </a></h4><h4><i class="fa fa-check"></i> Еще вы можете отследть статус вашей заявки в списке всех заявок по №: <a href="/requests"> ссылка </a></h4></div>';
				
				
			}else if(array.type == 'error'){
				info += '<div class="alert alert-error alert-dismissable animation-fadeInQuick"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="fa fa-times-circle"></i>Ололо</h4> Трололо</div>';
				console.log('error');
			}
			//$("#post").addClass('animation-fadeInQuickInv');
			document.getElementById("post").innerHTML=info;
			$("#clickable-wizard").hide('300');
			$('#next4').attr("onclick","");
			console.log(array.value);
		    //alert(array.success.title);
		    /*if (array.info.length >= 1){
		    	for(i in array.info){
		    		console.log("Ok");
					info += '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="fa fa-times-circle"></i></h4> </div>';
		    		
				}
			}
			**/
		})
		.fail(function() {
		    alert( "error" );
		});
	}else{
		console.log('Примите правила');
	}
	

}
function endInfo(){
	var addres = '';
	var problem = '';
	var array = new Object();
	var text = new Object();
	var body = '';
	array.asproblem = new Array();
	text.build = document.getElementsByName('val_build')[0].nextSibling.innerText;
	text.level = document.getElementsByName('val_level')[0].nextSibling.innerText;
	text.room = document.getElementsByName('val_room')[0].nextSibling.innerText;
	text.problem = document.getElementsByName('val_problem')[0].nextSibling.innerText;
	
	array.name = document.getElementById('val_username').value;
	array.email = document.getElementById('val_email').value;
	array.phone = document.getElementById('val_phone').value;
	array.phoneip = document.getElementById('val_phoneip').value;
	array.build = document.getElementById('val_build').value;
	array.level = document.getElementById('val_level').value;
	array.room = document.getElementById('val_room').value;
	array.problem = document.getElementById('val_problem').value;
	array.info = document.getElementById('val_info').value;
	
	/* Получение выбранных селектов */
	var select = document.getElementsByName('problem-checkbox');
	var i=0;
	select.forEach(function(element){
		
		if(element.checked){
			if(i < 1){
				array.asproblem[i] = element.value;
				text.asproblem = element.nextSibling.nodeValue;
			}else{
				array.asproblem[i] = element.value;
				text.asproblem += ', '+element.nextSibling.nodeValue;
			}
			i++;
			//console.log(element.nextSibling);
			//console.log(text.asproblem);
		}
		
		
	});
	/*   */
	if(array.name){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>ФИО</strong></td><td>'+array.name+'</td></tr>'
	}
	if(array.email){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Email</strong></td><td>'+array.email+'</td></tr>'
	}
	if(array.phone){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Мобильный телефон</strong></td><td>'+array.phone+'</td></tr>'
	}
	if(array.phoneip){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Рабочий телефон</strong></td><td>'+array.phoneip+'</td></tr>'
	}
	if(array.build){
		addres += text.build+' ';
	}
	if(array.level){
		addres += text.level+' этаж ';
	}
	if(array.room){
		addres += text.room+' кабинет';
	}
	if(addres){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Ваше расположение</strong></td><td>'+addres+'</td></tr>'
	}
	if(array.problem){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Тип заявки</strong></td><td>'+text.problem+'</td></tr>'
	}
	if(array.asproblem){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Причина заявки</strong></td><td>'+text.asproblem+'</td></tr>'
	}
	if(array.info){
		body += '<tr><td class="text-right" style="width: 50%;"><strong>Комментарий</strong></td><td>'+array.info+'</td></tr>'
	}
	
	
	document.getElementById('end_info').innerHTML=body;
	console.log(array);
	
}
function level_get(val){
	if(arr.building){
		var level_select = '<option></option>';
		arr.building.forEach(function(element) {
	    	if(val == element.id){
	    		for(var i = 1;  i <= element.level; i++){
					level_select += '<option value="'+i+'">'+i+'</option>';
					//console.log(element.level);
				}
				
			}
		});
	}
	document.getElementById("val_level").innerHTML=level_select;
}
function room_get(val){
	
	if(arr.room){
		var room_select = '<option></option>';
		var builds = document.getElementById('val_build').value;
		console.log(builds);
	
		arr.room.forEach(function(element) {
			
	    	if(val == element.flor & builds == element.housing ){
	    		console.log(val);
	    		//for(var i = 1;  i <= element.level; i++){
				room_select += '<option value="'+element.id+'">'+element.number+'</option>';
				//	console.log(element.level);
				//}
				
			}
		});
	}
	document.getElementById("val_room").innerHTML=room_select;
}
function lol(){
	 /* Initialize Advanced Wizard with Validation */
            $('#clickable-wizard').formwizard({
                disableUIStyles: false,
                validationEnabled: true,
                validationOptions: {
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function(e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        val_username: {
                            required: true,
                            minlength: 2
                        },
                        val_password: {
                            required: true,
                            minlength: 5
                        },
                        val_confirm_password: {
                            required: true,
                            equalTo: '#val_password'
                        },
                        val_email: {
                            required: true,
                            email: true
                        },
                        val_terms: {
                            required: true
                        },
                        val_select: {
							 required: true
						},
						val_build: {
							 required: true
						},
						val_level: {
							 required: true
						},
						val_phone: {
							 required: true
						},
						val_room: {
							 required: true
						}
                    },
                    messages: {
                        val_username: {
                            required: 'Введите фамилию, имя, отчество!',
                            minlength: 'Слишком мало символов'
                        },
                        val_password: {
                            required: 'Введите пароль!',
                            minlength: ''
                        },
                        val_confirm_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long',
                            equalTo: 'Please enter the same password as above'
                        },
                        val_email: 'Введите корректный email адрес!',
                        val_select: 'Выберите причину обраения!',
                        val_terms: 'Пожалуйста ознакомьтесь и примите правила!',
                        val_build: 'Выберите корпус',
                        val_level: 'Выберите этаж',
                        val_room: 'Выберите комнату',
                        val_phone: 'Введите номер телефона'
                    }
                },
                inDuration: 0,
                outDuration: 0
            });

	

	
}
/*$('#input_sms').on('click', function(){
                //var gotostep = $(this).data('gotostep');

                //clickableWizard.formwizard('show', gotostep);
                var sms = document.getElementById('input_sms');
                if(sms.checked){
					document.getElementById('sms').style.display = "block";
					document.getElementById("val_phone").required = true;		
				}else{
					document.getElementById('sms').style.display = "none";	
					document.getElementById("val_phone").required = false;		
				}
				console.log(sms.checked);
            	
            });*/
            

lol();
 $('#val_phone').mask('9 (999) 999-9999');
 $('#val_phoneip').mask('9999');
 
