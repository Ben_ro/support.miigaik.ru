var href = $(location).attr('pathname');
function typeGet (){
	var myValues = $('#viev').val();

	$.get({
		url: href+'/type/get',
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res) {
		const array = JSON.parse(res);
		array.forEach(function(element) {
			var html = '<tr>';
				html += '<td id="tname_'+element.id+'"><b>'+element.name+'</b></td>';
				html += '<td class="text-center" style="width: 70px;">';
					html += '<div class="btn-group btn-group-xs" >';
						html += '<a href="javascript:typeEdit('+element.id+')" class="btn btn-default"><i class="hi hi-pencil"></i></a>';
						html += '<a href="javascript:modal(\'type\', '+element.id+')" class="btn btn-default"><i class="hi hi-remove text-danger"></i></a>';
					html += '</div>';
				html += '</td>';
			html += '</tr>';
			$('#type-list').append(html);
		});
		var add = '<table class="table table-striped table-vcenter" id="type-list-add">';
		add += '<tbody>';
			add += '<tr>';
				add += '<td>';
					add += '<input type="text" id="type" name="name" class="form-control var_user ui-wizard-content ui-helper-reset ui-state-default" autocomplete="off" placeholder="Название" required="" aria-required="true">';
				add += '</td>';
				
				add += '<td class="text-center" style="width: 70px;">';
					add += '<div class="btn-group btn-group-xs">';
						add += '<a href="javascript:typeAdd()" class="btn btn-default"><i class="fa fa-check"></i></a>';
					add += '</div>';
				add += '</td>';
			add += '</tr>';
		add += '</tbody>';
		add += '</table>';
		$('#type-list').after(add);
	})
}
function typeEdits(id){
	var table = '<div id="asp" style="display:none">';
		table += '<a href="javascript:back()" class="btn btn-default" id="back" style="padding: 4px 12px;margin: 10px 0;"><i class="gi gi-undo"></i></a>';
		table += '<table class="table table-striped table-vcenter" id="astype-list"></table>';
	table += '</div>';
	$('#type-list').after(table); 
	$.get({
		url: href+'/astype/get?id='+id,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res) {
		const array = JSON.parse(res);
		array.forEach(function(element) {
			var html = '<tr>';
				html += '<td id="asname_'+element.id+'"><b>'+element.name+'</b></td>';
				html += '<td id=""><b>'+element.type+'</b></td>';
				html += '<td class="text-center" style="width: 70px;">';
					html += '<div class="btn-group btn-group-xs" >';
						html += '<a href="javascript:astypeEdit('+element.id+', \''+element.name+'\')" class="btn btn-default" id="edit_'+element.id+'"><i class="hi hi-pencil"></i></a>';
						html += '<a href="javascript:modal(\'astype\', '+element.id+')" class="btn btn-default"><i class="hi hi-remove text-danger"></i></a>';
					html += '</div>';
				html += '</td>';
			html += '</tr>';
			$('#astype-list').append(html);
		});
		var add = '<table class="table table-striped table-vcenter" id="astype-list-add">';
		add += '<tbody>';
			add += '<tr>';
				add += '<td>';
					add += '<input type="text" id="astype" name="name" class="form-control ui-wizard-content ui-helper-reset ui-state-default" autocomplete="off" placeholder="Название" required="" aria-required="true">';
				add += '</td>';
				add += '<td>';
					add += '<label class="checkbox-inline" for="label"><input type="radio" id="label" name="problem-checkbox" value="label">checkbox</label>';
					add += '<label class="checkbox-inline" for="input"><input type="radio" id="input" name="problem-checkbox" value="input">input</label>';
				add += '</td>';
				
				add += '<td class="text-center" style="width: 70px;">';
					add += '<div class="btn-group btn-group-xs">';
						add += '<a href="javascript:astypeAdd('+id+')" class="btn btn-default"><i class="fa fa-check"></i></a>';
					add += '</div>';
				add += '</td>';
			add += '</tr>';
		add += '</tbody>';
		add += '</table>';
		$('#astype-list').after(add);
	})
	$('#asp').fadeIn(300);
}
function typeEdit(id){
	$('#type-list').fadeOut(300);
	$('#type-list-add').fadeOut(300);
	setTimeout(typeEdits, 305, id);
}
function deleted(){
	$('#asp').remove();
	$('#type-list').fadeIn(300);
	$('#type-list-add').fadeIn(300);
}
function back(){
	$('#asp').fadeOut(300);
	setTimeout(deleted,305);
}
function astypeEdit(id, name){
	var input = '<input type="text" id="name_'+id+'" name="name" class="form-control var_user ui-wizard-content ui-helper-reset ui-state-default" autocomplete="off" value="'+name+'" required="" aria-required="true">';
	$('#asname_'+id).html(input)
	var value = $('#name_'+id).val();
	$('#edit_'+id).attr('id','save_'+id);
	$('#save_'+id).attr('href','javascript:astypeSave('+id+')');
	$('#save_'+id).html('<i class="fa fa-check"></i>');
}
function astypeSave(id){
	var value = $('#name_'+id).val();
	var href = $(location).attr('pathname');
	$.get({
		url: href+'/astype/edit?id='+id+'&name='+value,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res){
		if(res == 1){
			var input = '<b>'+value+'</b>';
			$('#asname_'+id).html(input);
			$('#save_'+id).attr('id','edit_'+id);
			$('#edit_'+id).attr('href','javascript:javascript:astypeEdit('+id+', \''+value+'\')');
			$('#edit_'+id).html('<i class="hi hi-pencil"></i>');
		}else{
			
		}
	})
}
function astypeAdd(id){
	var value = $('#astype').val();
	var type = $('input[name=problem-checkbox]:checked').val();
	console.log(type)
	if(value != '' && type){
		$.get({
			url: href+'/astype/add?name='+value+'&problem='+id+'&checkbox='+type,
			xhrFields: {
				credentials: 'include',
			},
		}).done(function(res){
			const array = JSON.parse(res);
			if(res != ''){
				var html = '<tr>';
					html += '<td id="asname_'+array.id+'"><b>'+value+'</b></td>';
					html += '<td id=""><b>'+array.type+'</b></td>';
					html += '<td class="text-center" style="width: 70px;">';
						html += '<div class="btn-group btn-group-xs" >';
							html += '<a href="javascript:astypeEdit('+array.id+', \''+value+'\')" class="btn btn-default" id="edit_'+array.id+'"><i class="hi hi-pencil"></i></a>';
							html += '<a href="javascript:modal(\'astype\', '+array.id+')" class="btn btn-default"><i class="hi hi-remove text-danger"></i></a>';
						html += '</div>';
					html += '</td>';
				html += '</tr>';
				$('#astype-list').append(html);
				$('#astype').val('')
			}
		})
	}
}
function astypeDel(id){
	
	$.get({
		url: href+'/astype/del?id='+id,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res){
		if(res == 1){
			$('#asname_'+id).parent().remove()
		}
	})
	closeModal()	
}
function typeDel(id){
	
	$.get({
		url: href+'/type/del?id='+id,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res){
		if(res == 1){
			$('#tname_'+id).parent().remove()
		}
	})
	closeModal()	
}
function typeAdd(){
	var value = $('#type').val();
	if(value != ''){
		$.get({
			url: href+'/type/add?name='+value,
			xhrFields: {
				credentials: 'include',
			},
		}).done(function(res){
			const array = JSON.parse(res);
			if(res != ''){
				var html = '<tr>';
					html += '<td id="tname_'+array.id+'"><b>'+value+'</b></td>';
					html += '<td class="text-center" style="width: 70px;">';
						html += '<div class="btn-group btn-group-xs" >';
							html += '<a href="javascript:typeEdit('+array.id+', \''+value+'\')" class="btn btn-default" id="edit_'+array.id+'"><i class="hi hi-pencil"></i></a>';
							html += '<a href="javascript:modal(\'type\', '+array.id+')" class="btn btn-default"><i class="hi hi-remove text-danger"></i></a>';
						html += '</div>';
					html += '</td>';
				html += '</tr>';
				$('#type-list').append(html);
				$('#type').val('')
			}
		})
	}
}
function infoSave(id){
	var value = $('#info_'+id).val();
	$.get({
		url: href+'/info/edit?text='+value+'&dep='+id,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res){
		var input = value;
		$('#editinfo_'+id).html(input);
		$('#saveinfob_'+id).attr('id','editinfob_'+id);
		$('#editinfob_'+id).attr('href','javascript:javascript:infoEdit('+id+', \''+value+'\')');
		$('#editinfob_'+id).html('<i class="hi hi-pencil"></i>');
	})
}
function addressSave(id){
	var value = $('#address_'+id).val();
	$.get({
		url: href+'/address/edit?text='+value+'&dep='+id,
		xhrFields: {
			credentials: 'include',
		},
	}).done(function(res){
		var input = value;
		$('#editaddress_'+id).html(input);
		$('#saveaddressb_'+id).attr('id','editaddressb_'+id);
		$('#editaddressb_'+id).attr('href','javascript:javascript:addressEdit('+id+', \''+value+'\')');
		$('#editaddressb_'+id).html('<i class="hi hi-pencil"></i>');
	})
}
function infoEdit(id, value){
	var input = '<input type="text" id="info_'+id+'" name="name" class="form-control var_user ui-wizard-content ui-helper-reset ui-state-default" autocomplete="off" value="'+value+'" required="" aria-required="true">';
	$('#editinfo_'+id).html(input)
	$('#editinfob_'+id).attr('id','saveinfob_'+id);
	$('#saveinfob_'+id).attr('href','javascript:infoSave('+id+', \''+value+'\')');
	$('#saveinfob_'+id).html('<i class="fa fa-check"></i>');
}
function addressEdit(id, value){
	var input = '<input type="text" id="address_'+id+'" name="name" class="form-control var_user ui-wizard-content ui-helper-reset ui-state-default" autocomplete="off" value="'+value+'" required="" aria-required="true">';
	$('#editaddress_'+id).html(input)
	$('#editaddressb_'+id).attr('id','saveaddressb_'+id);
	$('#saveaddressb_'+id).attr('href','javascript:addressSave('+id+')');
	$('#saveaddressb_'+id).html('<i class="fa fa-check"></i>');
}
function modal(type, id){
	if(type == 'astype'){
		var name = $('#asname_'+id).text()
		var text = 'Вы точно хотите удалить подтип заявки "'+name+'"?'
	}else{
		var name = $('#tname_'+id).text()
		var text = 'Вы точно хотите удалить тип заявки "'+name+'", вместе с ее содержимым?'
	}
	
	var modal = '<div id="modal-regular2" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">'
	modal +='<div class="modal-dialog">'
	modal +='<div class="modal-content">'
	modal +='<div class="modal-header">'
	modal +='<button type="button" onclick="closeModal()" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
	modal +='<h3 class="modal-title">Удалить тип заявки</h3>'
	modal +='</div>'
	modal +='<div class="modal-body">'
	modal += text
	modal +='</div>'
	modal +='<div class="modal-footer">'
	modal +='<button type="button" onclick="closeModal()" class="btn btn-sm btn-default" data-dismiss="modal">Нет</button>'
	modal +='<button type="button" onclick="'+type+'Del('+id+')" class="btn btn-sm btn-primary">Да</button>'
	modal +='</div>'
	modal +='</div>'
	modal +='</div>'
	modal +='</div>'
	
	$('body').append(modal)
	$('#modal-regular2').show()
}
function closeModal(){
	$('#modal-regular2').hide()
	$('#modal-regular2').remove()
}

