/*
 *  Document   : ecomOrders.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in eCommerce Orders page
 */

function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.problem+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.status+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}
function departmentClose(id){ 
	console.log($('#del-'+id).prev());
	$.post('/panel/department/del', {
			_token: $('meta[name=csrf-token]').attr('content'),
			id: id
			}
		)
		.done(function(data) {
			$('#dep-'+id).remove();
			setTimeout(function() {
				$('#dep-'+id).fadeOut('fast');
			}, 1000);
			
		})
		.fail(function() {
		    alert( "error" );
		});
}
var adminRequests = function() {

    return {
        init: $(document).ready(function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
			$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
			    switch ( d ) {
			        case '<center><span class="label label-info">Новая</span></center>':    return 1;
			        case '<center><span class="label label-warning">В работе</span></center>': return 2;
			        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
			    }
			    return 0;
			};
            /* Initialize Datatables */
            var table = $('#ecom-orders').dataTable({
            	ajax: '/api/admin/requests',
            	stateSave: true,
            	columns: [
		            {
		                "className":      'details-control',
		                "orderable":      false,
		                "data":           null,
		                "defaultContent": ''
		            },
		            { "data": "idd" },
		            { "data": "problem" },
		            { "data": "status" },
		            {
		                "className":      'edit',
		                "orderable":      false,
		                "data":           null,
		                "defaultContent": '<request-edit></request-edit>'
		            },
		            { "data": "created_at" }
		        ],
                columnDefs: [
                    { orderable: false, type: 'date-custom', targets: [4]},
                    {type: 'salary-grade', targets: [3]}
                ],
                order: [[ 1, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });
			$('#ecom-orders tbody').on('click', 'td.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = table.row( tr );
		 
		        if ( row.child.isShown() ) {
		            // This row is already open - close it
		            row.child.hide();
		            tr.removeClass('shown');
		        }
		        else {
		            // Open this row
		            row.child( format(row.data()) ).show();
		            tr.addClass('shown');
		        }
		    } );
            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        })
    };
}();

$('.task-close').click(function (){
	console.log('lol');
	$(this).parent().remove();
})
