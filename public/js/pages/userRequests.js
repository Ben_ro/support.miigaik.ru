/*
 *  Document   : ecomOrders.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in eCommerce Orders page
 */

function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.problem+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}

function newRequet(){
	$('#workRequets_wrapper').hide();
	$('#closeRequets_wrapper').hide();
	$('#newRequets_wrapper').show();
}
function workRequet(){
	$('#workRequets_wrapper').show();
	$('#closeRequets_wrapper').hide();
	$('#newRequets_wrapper').hide();
}
function closeRequet(){
	$('#workRequets_wrapper').hide();
	$('#closeRequets_wrapper').show();
	$('#newRequets_wrapper').hide();
}
var newRequets = function() {

    return {
        init: $(document).ready(function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
			$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
			    switch ( d ) {
			        case '<center><span class="label label-info">Новая</span></center>':    return 1;
			        case '<center><span class="label label-warning">В работе</span></center>': return 2;
			        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
			    }
			    return 0;
			};
            /* Initialize Datatables */
            var table = $('#newRequets').dataTable({
            	ajax: '/panel/user/requests/1',
            	stateSave: true,
            	columns: [
		            { "data": "id" },
		            { "data": "problemHtml" },
		            { "data": "created_at" }
		        ],
                columnDefs: [
                    {type: 'salary-grade', targets: [2]}
                ],
                order: [[ 1, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });
			$('#newRequets tbody').on('click', 'td.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = table.row( tr );
		 
		        if ( row.child.isShown() ) {
		            // This row is already open - close it
		            row.child.hide();
		            tr.removeClass('shown');
		        }
		        else {
		            // Open this row
		            row.child( format(row.data()) ).show();
		            tr.addClass('shown');
		        }
		    } );
            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Поиск');
        })
    };
}();
var workRequets = function() {

    return {
        init: $(document).ready(function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
			$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
			    switch ( d ) {
			        case '<center><span class="label label-info">Новая</span></center>':    return 1;
			        case '<center><span class="label label-warning">В работе</span></center>': return 2;
			        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
			    }
			    return 0;
			};
            /* Initialize Datatables */
            var table = $('#workRequets').dataTable({
            	ajax: '/panel/user/requests/2',
            	stateSave: true,
            	columns: [
					{ "data": "id" },
		            { "data": "problemHtml" },
		            { "data": "created_at" }
		        ],
                columnDefs: [
                    {type: 'salary-grade', targets: [2]}
                ],
                order: [[ 1, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });
			$('#workRequets tbody').on('click', 'td.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = table.row( tr );
		 
		        if ( row.child.isShown() ) {
		            // This row is already open - close it
		            row.child.hide();
		            tr.removeClass('shown');
		        }
		        else {
		            // Open this row
		            row.child( format(row.data()) ).show();
		            tr.addClass('shown');
		        }
		    } );
            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Поиск');
        })
    };
}();
var closeRequets = function() {

    return {
        init: $(document).ready(function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
			$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
			    switch ( d ) {
			        case '<center><span class="label label-info">Новая</span></center>':    return 1;
			        case '<center><span class="label label-warning">В работе</span></center>': return 2;
			        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
			    }
			    return 0;
			};
            /* Initialize Datatables */
            var table = $('#closeRequets').dataTable({
            	ajax: '/panel/user/requests/3',
            	stateSave: true,
            	columns: [
		            { "data": "id" },
		            { "data": "problemHtml" },
		            { "data": "created_at" }
		        ],
                columnDefs: [
                    {type: 'salary-grade', targets: [2]}
                ],
                order: [[ 1, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });
			$('#closeRequets tbody').on('click', 'td.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = table.row( tr );
		 
		        if ( row.child.isShown() ) {
		            // This row is already open - close it
		            row.child.hide();
		            tr.removeClass('shown');
		        }
		        else {
		            // Open this row
		            row.child( format(row.data()) ).show();
		            tr.addClass('shown');
		        }
		    } );
            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Поиск');
        })
    };
}();
$(function () {
    $('#workRequets_wrapper').hide();
	$('#closeRequets_wrapper').hide();
 });

$('.task-close').click(function (){
	console.log('lol');
	$(this).parent().remove();
})
