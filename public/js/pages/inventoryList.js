function filterColumn ( i ) {
    $('#ecom-orders').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
function updateStatus(value, id){
	var status = '';
	switch(value) {
		case '0':
			status = 'Активно';
			break;
		case '1':
			status = 'На ремонте';
			break;
		case '2':
			status = 'Списан';
			break;
	}
	$('#status-'+id).after('<i class="fa fa-spinner fa-2x fa-spin" id="load-'+id+'"></i>');
	$.post('/panel/inventory/updateStatus', {
			_token: $('meta[name=csrf-token]').attr('content'),
			id: id,
			value: value
			}
		)
		.done(function(data) {
			$('#load-'+id).remove();
			$('#status-'+id).after("<i class='hi hi-ok' id='ok-"+id+"' style='font-size: 20px;color: #00de09;'></i>");
			setTimeout(function() {
				$('#ok-'+id).fadeOut('fast');
			}, 1000);
			setTimeout(function() {
				$('#ok-'+id).remove();
			}, 1500);
			
		})
		.fail(function() {
		    alert( "error" );
		});
}
var InventoryList = function() {

    return {
        init: function() {
        	$('input.column_filter').on( 'keyup click', function () {
		        filterColumn( $(this).parents('tr').attr('data-column') );
		    } );
		    $('select.column_filter').on( 'keyup click', function () {
		        filterColumn( $(this).parents('tr').attr('data-column') );
		    } );
		            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );
			
            /* Initialize Bootstrap Datatables Integration */
            App.datatables(); 
			/*$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
			    switch ( d ) {
			        case '<center><span class="label label-info">Новая</span></center>':    return 1;
			        case '<center><span class="label label-warning">В работе</span></center>': return 2;
			        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
			    }
			    return 0;
			};*/
            /* Initialize Datatables */
			var hello = 'Привет';
            $('#inventoryList').dataTable({
            	ajax: '/panel/inventory/listGet',
            	stateSave: true,
            	columns: [
		            { "data": "inventory_id" },
		            { "data": "type" },
		            { "data": "housing" },
		            { "data": "room" },
		            { "data": "info" },
		            { "data": "status" },
		            { "data": 'counts' }
		        ],
				columnDefs: [
                    {orderable: false, type: 'date-custom', targets: [3]},
                    {type: 'salary-grade', targets: [2]}
                ],
				columnDefs: [
                    {className: "hidden-sm hidden-xs", targets: [2,4]},
					{className: "hidden-xs", targets: [3]}
                ],
                order: [[ 0, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
