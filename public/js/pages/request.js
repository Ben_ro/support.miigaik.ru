/*window.Echo.channel('test-event')
    .listen('MassageEvent', (e) => {
        console.log(e);
    });*/
function SendMassage(){
	if(document.getElementById('user_id').value == ''){
		var user_id = 0;
	}else{
		var user_id = document.getElementById('user_id').value;
	}
	
	$.post('/chat/send', {
			_token: $('meta[name=csrf-token]').attr('content'),
			id: document.getElementById('id').value,
			user_id: user_id,
			text:document.getElementById('profile-tweet').value,
			//newLng: newLng
			}
		)
		.done(function(data) {
			alert( "Отправилось!" );
			
		})
		.fail(function() {
		    alert( "error" );
		});
}
function updateStatus(status){
	$.post('/api/request/status', {
			_token: $('meta[name=csrf-token]').attr('content'),
			request_id: document.getElementById('id').value,
			status_id: status
			//newLng: newLng
			}
		)
		.done(function(data) {
			requestLog()
		})
		.fail(function() {
		    alert( "error" );
		});
}
function removeWorker(worker, type){
	$.post('/api/request/worker/del', {
			_token: $('meta[name=csrf-token]').attr('content'),
			request_id: document.getElementById('id').value,
			worker_id: worker,
			worker_type: type
			}
		)
		.done(function(data) {
			$('.worker'+worker).remove();
			requestLog()
		})
		.fail(function() {
		    alert( "error" );
		});
}
function addWorker(worker, type){

	$.post('/api/request/worker/add', {
			_token: $('meta[name=csrf-token]').attr('content'),
			request_id: document.getElementById('id').value,
			worker_id: worker,
			worker_type: type
			}
		)
		.done(function(data) {
			//console.log(data)
			var array = JSON.parse(data);
			if(array.id){
				var work = [array]
				if(type == 1){
					addSelect(work, '#worker', type, 'html', 1)
					$('#work_none').remove();
				}else if(type == 2){
					addSelect(work, '#soworker', type, 'html', 1)
					$('#sowork_none').remove();
				}
			}
			requestLog();
		})
		.fail(function() {
		    alert( "error" );
		});
}
function addSelect(array, id, type, select, eworker){
	if(select == 'select'){
		var html = ''
		if(array.length){
			html += '<option value=""></option>'
			$.each(array, function( key, value ) {
				html += '<option value="'+value.id+'" >'+value.fio+'</option>'
			});
		}else{
			html += '<option value="">Нет сотрудников</option>'
		}
		$(id).append(html);
	}else if(select = 'html'){
		var html = '';
				$.each(array, function( key, value ) {
					html +='<div class="block-section worker'+value.id+'">'
						html +='<table class="table table-borderless table-striped table-vcenter">'
							html +='<tr>'
								html +='<td class="text-right"></td>'
								html +='<td>'
									if(type ==1){
									html +='<strong>Исполнитель заявки</strong> '
									}else if(type == 2){
										html +='<strong>Соисполнитель заявки</strong> '
									}
								html +='</td>'
							html +='</tr>'	
							html +='<tr>'
								html +='<td class="text-right" style="width: 30%;">'
									html +='<strong> ФИО</strong>'
									html +=''
								html +='</td>'
								html +='<td><strong>'+value.fio+'</strong></td>'
							html +='</tr>'
							html +='<tr>'
								html +='<td class="text-right">Контакты</td>'
								html +='<td>'
									if(value.email){
									html +='<i class="fa fa-envelope fa-fw text-success"></i><strong>Email:</strong> '+value.email+'<br>'
									}
									if(value.phone){
									html +='<i class="hi hi-earphone fa-fw text-success"></i><strong>Телефон:</strong> '+value.phone
									}
								html +='</td>'
							html +='</tr>'
							if(eworker){
							html +='<tr><td colspan="2"><div class="closed"><button type="button" class="btn btn-block btn-danger" onclick="removeWorker('+value.id+', '+type+')">Удалить</button></div></td></tr>'
							}
						html +='</table>'
					html +='</div>'
				});
				$(id).append(html);
	}
}
function requestLog(){
	$.post('/api/request/log', {
			_token: $('meta[name=csrf-token]').attr('content'),
			request_id: document.getElementById('id').value,
			}
		)
		.done(function(data) {
			//console.log(data)
			var array = JSON.parse(data)
			$('#log').html('');
			//console.log(array);
			$.each(array, function( key, value ) {
				
				var html = ''
				html += '<li>';
				html += '<div class="timeline-icon">'+value.type+'</div>';
				html += '<div class="timeline-time">'+value.times+'</div>';
				html += '<div class="timeline-content">';
				html += '<p class="push-bit"><strong>'+value.user+'</strong></p>';
				html += '<p class="push-bit">'+value.text+'</p>';
				html += '</div>';
				html += '</li>';
				console.log(html);
				$('#log').append(html)
			});
		})
		.fail(function() {
		    alert( "error" );
		});
}
function get_info(){
	$.post('/api/request', {
			_token: $('meta[name=csrf-token]').attr('content'),
			val_id: document.getElementById('id').value
			//newLng: newLng
			}
		)
		.done(function(data) {
			var array = JSON.parse(data);
			var i = 0;
			var info = '';
			var body = '';
			var addres = '';
			var status = '';
			
			console.log(array);
			if(array.user){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>ФИО</strong></td><td>'+array.user+'</td></tr>'
			}
			if(array.email){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Email</strong></td><td>'+array.email+'</td></tr>'
			}
			if(array.phone){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Телефон</strong></td><td>'+array.phone+'</td></tr>'
			}
			if(array.phoneip){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Рабочий телефон</strong></td><td>'+array.phoneip+'</td></tr>'
			}
			if(array.build){
				addres += array.build+' ';
			}
			if(array.level){
				addres += array.level+' этаж ';
			}
			if(array.room){
				addres += array.room+' кабинет';
			}
			if(addres){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Расположение</strong></td><td>'+addres+'</td></tr>'
			}
			if(array.problem){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Причина заявки</strong></td><td>'+array.problem+'</td></tr>'
			}
			if(array.asproblem){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Дополнение</strong></td><td>'+array.asproblem+'</td></tr>'
			}
			if(array.info){
				body += '<tr><td class="text-right" style="width: 50%;"><strong>Комментарий</strong></td><td>'+array.info+'</td></tr>'
			}
			
			if(array.worker.length){
				addSelect(array.worker, '#worker', 1, 'html', array.eworker)
				
			}else{
				var html = '';
				html +='<div class="block-section" id="work_none">'
						html +='<table class="table table-borderless table-striped table-vcenter">'
							html +='<tr>'
								html +='<td class="text-right"></td>'
								html +='<td>'
									html +='<strong>Исполнитель еще не назначен</strong>'
								html +='</td>'
							html +='</tr>'	
						html +='</table>'
					html +='</div>'
					$('#worker').append(html);
			}
			if(array.soworker.length){
				addSelect(array.soworker, '#soworker', 2, 'html', array.eworker)
			}else{
				var html = '';
				html +='<div class="block-section"id="sowork_none">'
						html +='<table class="table table-borderless table-striped table-vcenter">'
							html +='<tr>'
								html +='<td class="text-right"></td>'
								html +='<td>'
									html +='<strong>Соисполнитель еще не назначен</strong>'
								html +='</td>'
							html +='</tr>'	
						html +='</table>'
					html +='</div>'
					$('#soworker').append(html);
			}
			
			if(array.status){
				
				if(array.estatus){
					console.log(array.estatus)
					var neew = '';
					var work = '';
					var close = '';
					if(array.status.indexOf('Новая') != '-1'){
						neew = 'selected';
					}else if(array.status.indexOf('В работе') != '-1'){
						work = 'selected';
					}else if(array.status.indexOf('Закрыта') != '-1'){
						close = 'selected';
					}
					status += '<option value="1" '+neew+'>Новая</option>'
					status += '<option value="2" '+work+'>В работе</option>'
					status += '<option value="3" '+close+'>Закрыта</option>'
					$('#statuschange').html(status);
					$('#status').html('');
				}else{
					status += '<span class="h2 text-info animation-expandOpen">'+array.status+'</span>';
					$('#status').html(status);
				}
			}
			if(array.eworker){
				addSelect(array.wdepartment, '#selectw', 1, 'select')
				addSelect(array.wdepartment, '#selectsw', 2, 'select')
			}
			document.getElementById('end_info').innerHTML=body;
			//document.getElementById('status').innerHTML=status;
			requestLog();
			
		})
		.fail(function() {
		    alert( "error" );
		});
}
get_info();