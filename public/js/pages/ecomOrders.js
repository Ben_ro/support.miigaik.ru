/*
 *  Document   : ecomOrders.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in eCommerce Orders page
 */

var EcomOrders = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
                "date-custom-pre": function ( a ) {
                    var customDate = a.split('/');
                    return (customDate[2] + customDate[1] + customDate[0]) * 1;
                },

                "date-custom-asc": function ( a, b ) {
                    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
                },

                "date-custom-desc": function ( a, b ) {
                    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
                }
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
			$.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
    switch ( d ) {
        case '<center><span class="label label-info">Новая</span></center>':    return 1;
        case '<center><span class="label label-warning">В работе</span></center>': return 2;
        case '<center><span class="label label-success">Закрыта</span></center>':   return 3;
    }
    return 0;
};
            /* Initialize Datatables */
            $('#ecom-orders').dataTable({
            	ajax: '/api/requests/all',
            	stateSave: true,
            	columns: [
		            { "data": "idd" },
		            { "data": "problem" },
		            { "data": "status" },
		            { "data": "created_at" }
		        ],
                columnDefs: [
                    { orderable: false, type: 'date-custom', targets: [3]},
                    {type: 'salary-grade', targets: [2]}
                ],
                order: [[ 0, "desc" ]],
                pageLength: 20,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();