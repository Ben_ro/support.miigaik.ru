@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<!-- Clickable Wizard Block -->
                        <div class="block">
                            <!-- Clickable Wizard Title -->
                            <div class="block-title">
                                <h2><strong>Оформление</strong> Заявки</h2>
                            </div>
                            <!-- END Clickable Wizard Title -->
							<div id="post"></div>
                            <!-- Clickable Wizard Content -->
                            <form id="clickable-wizard"  method="post" class="form-horizontal form-bordered"  onsubmit="return false;">
                                <!-- First Step -->
                                <div id="clickable-first" class="step">
                                    <!-- Step Info -->
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <ul class="nav nav-pills nav-justified clickable-steps">
                                                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>1. Информация о Вас</strong></a></li>
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>2. Информация о проблеме</strong></a></li>
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Итоговая информация</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- END Step Info -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="val_username">ФИО</label>
                                        <div class="col-md-5">
                                         	<input type="text" id="val_username" name="val_username" class="form-control var_user" autocomplete="off" placeholder="Фамилия Имя Отчество" required>  
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="val_email">Email</label>
                                        <div class="col-md-5">
                                            <input type="text" id="val_email" name="val_email" class="form-control" placeholder="test@example.com">
                                        </div>
                                    </div>
                                    <div class="form-group" id="sms">
                                   		<label class="col-md-4 control-label" for="val_phone">Номер мобильного телефона</label>
                                        <div class="col-md-5">
                                            <input type="text" id="val_phone" name="val_phone" class="form-control" placeholder="9 (999) 999-9999">
                                        </div>
                                   </div>
                                    <div class="form-group" id="sms">
                                   		<label class="col-md-4 control-label" for="val_phoneip">Номер рабочего телефона</label>
                                        <div class="col-md-5">
                                            <input type="text" id="val_phoneip" name="val_phoneip" class="form-control" placeholder="9999">
                                        </div>
                                   </div>
                                    <fieldset>
                                        <legend><i class="fa fa-angle-right"></i> Ваше расположение</legend>
                                    	<div class="form-group">
	                                        <label class="col-md-4 control-label" for="example-clickable-firstname">Выберите корпус</label>
	                                        	<div class="col-md-5">
	                                        		<select id="val_build" name="val_build" class="select-select2" style="width: 100%;" data-placeholder="Choose one.."  onChange="level_get(this.value)"  aria-required="true"></select>
		                                        </div>
                                    	</div>
	                                    <div class="form-group">
	                                    	<label class="col-md-4 control-label" for="example-clickable-firstname">Выберите этаж</label><div class="col-md-5">
	                                        		<select id="val_level" name="val_level" class="select-select2" style="width: 100%;" data-placeholder="Choose one.."  onChange="room_get(this.value)"  aria-required="true"></select>
		                                        </div>
	                                    </div>
	                                     <div class="form-group">
	                                     	<label class="col-md-4 control-label" for="example-clickable-firstname">Выберите кабинет</label><div class="col-md-5">
	                                        		<select id="val_room" name="val_room" class="select-select2" style="width: 100%;" data-placeholder="Choose one.."   aria-required="true"></select>
		                                        </div>
	                                     </div>
                                    </fieldset>
                                     <fieldset>
                                            <legend><i class="fa fa-angle-right"></i> Дополнительные возможности</legend>
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Включить email оповещение о статусе заявки</label>
                                        <div class="col-md-1">
                                            <label class="switch switch-success"><input type="checkbox" id="val_emailsend" checked><span></span></label>
                                        </div>
                                        <label class="col-md-3 control-label">Включить sms оповещение о статусе заявки</label>
                                        <div class="col-md-2">
                                            <label class="switch switch-success"><input type="checkbox" id="val_phonesend"><span></span></label>
                                        </div>
                                    </div>
                                   
                                    </fieldset>
                                    
                                </div>
                                <!-- END First Step -->

                                <!-- Second Step -->
                                <div id="clickable-second" class="step">
                                    <!-- Step Info -->
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <ul class="nav nav-pills nav-justified clickable-steps">
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-check"></i> <strong>1. Информация о Вас</strong></a></li>
                                                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>2. Информация о проблеме</strong></a></li>
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Итоговая информаци</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- END Step Info -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-clickable-firstname">Выберите тип заявки</label>
                                        <div class="col-md-5">
                                        
                                        <select id="val_problem" name="val_problem" class="select-select2" style="width: 100%;" data-placeholder="Choose one.." onChange="problem_get(this.value)"  aria-required="true"></select>
                                                    
                                        </div>
                                    </div>
                                    <div class="form-group" id="form-label" style="display: none">
                                    	<label class="col-md-4 control-label">Выберите причину (1 или более)</label>
                                            <div class="col-md-5" id="label"></div>
                                    </div>
                                    <div id="form-input"></div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="val_info">Комментарий к заявке</label>
                                        <div class="col-md-8">
                                            <textarea id="val_info" name="val_info" rows="6" class="form-control" placeholder="Введите свой комментарий"></textarea>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- END Second Step -->
                                

                                <!-- Third Step -->
                                <div id="clickable-third" class="step">
                                    <!-- Step Info -->
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <ul class="nav nav-pills nav-justified clickable-steps">
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><i class="fa fa-check"></i> <strong>1. Информация о Вас</strong></a></li>
                                                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><i class="fa fa-check"></i> <strong>2. Информация о проблеме</strong></a></li>
                                                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>3. Итоговая информаци</strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- END Step Info -->
                                    <!-- Quick Month Stats Content -->
                                    <table class="table table-striped table-borderless table-vcenter">
                                        <tbody id="end_info"> 
                                            <tr>
                                                <td class="text-right" style="width: 50%;">
                                                    <strong>ФИО</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Email</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Номер телефона</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Расположение</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Причина заявки</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Quick Month Stats Content -->
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><a href="#modal-terms" data-toggle="modal">Правила</a></label>
                                        <div class="col-md-8">
                                        	
                                            <label class="switch switch-success" for="val_terms">
                                                <input type="checkbox" id="val_terms" name="val_terms" value="1">
                                                <span data-toggle="tooltip" title="Я соглашюсь с правилами подачи заявок!"></span>
                                            </label>
                                        </div>
                                    </div>
                                     
                                </div>
                                <!-- END Third Step -->

                                <!-- Form Buttons -->
                                <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="reset" class="btn btn-sm btn-warning" id="back4">Назад</button>
                                        <label id="pnext"><button type="submit" class="btn btn-sm btn-primary" id="next4">Далее</button></label>
                                        
                                    </div>
                                </div>
                                <!-- END Form Buttons -->
                                <!-- Terms Modal -->
                        <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h3 class="modal-title"><i class="gi gi-pen"></i> Service Terms</h3>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="sub-header">1.1 | General</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.2 | Account</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.3 | Service</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.4 | Payments</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Ok, I've read them!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Terms Modal -->
                            </form>
                            <!-- END Clickable Wizard Content -->
                        </div>
                        <!-- END Clickable Wizard Block -->
@stop
@section('modal')

                        <!-- Terms Modal -->
                        <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h3 class="modal-title"><i class="gi gi-pen"></i> Service Terms</h3>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="sub-header">1.1 | General</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.2 | Account</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.3 | Service</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                        <h4 class="sub-header">1.4 | Payments</h4>
                                        <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Ok, I've read them!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Terms Modal -->
                        
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		<script src="{{ asset('js/pages/formsWizard.js')}}"></script>
        <script src="{{ asset('js/helpers/ckeditor/ckeditor.js')}}"></script>
@stop