@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
	<div class="block">
		<!-- Block Title -->
		<div class="block-title">
		    <h2><strong>Просмотр</strong> заявки</h2>
		</div>
		<!-- END Block Title -->
		<div class="form-group">
                            	@if(!empty($info))
                            		{{$info}}
	                            @endif
	                            
        </div>
		{!! Form::open(['url' => '/request', 'class' => 'form-horizontal form-bordered ui-formwizard', 'id' => 'clickable-wizard']) !!}
		
		<div class="form-group">
			{!! Form::label('val_id', 'Введите уникальный код заявки',['class' => 'col-md-4 control-label']) !!}
			<div class="col-md-5">
				{!! Form::text('val_id', '', ['class' => 'form-control ui-wizard-content', 'id' => 'val_id', 'placeholder' => 'Код заявки']) !!}
				
				<span class="help-block">(состоит из 5 символов)</span>
			</div>
		</div>
		<div class="form-group">
			
			<center>{!! Form::button('Найти', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']) !!}</center>
		</div>
		{!! Form::close() !!}
		
	</div>
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		<script src="{{ asset('js/pages/requestViev.js')}}"></script>
        <script src="{{ asset('js/helpers/ckeditor/ckeditor.js')}}"></script>
@stop