@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<div class="block full">
                            <!-- All Orders Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                </div>
                                <h2><strong>Все </strong> Заявки</h2>
                            </div>
                            <!-- END All Orders Title -->

                            <!-- All Orders Content -->
                            <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 100px;">ID</th>
                                        <th class="visible-lg">Проблема</th>
                                        <th class="text-center hidden-xs">Статус</th>
                                        <th>Время создания</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    
                                </tbody>
                            </table>
                            <!-- END All Orders Content -->
                            </div>
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		<script src="js/pages/ecomOrders.js"></script>
        <script>$(function(){ EcomOrders.init(); });</script>
@stop