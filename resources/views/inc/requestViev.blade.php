@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<input type='hidden' value='{{$id}}' id="id">
<div class="row">
	<div class="colum-md-6 col-lg-8">
		<div class="block">
			<div class="block-title">
			    <h2><strong>Данные</strong> о заявке</h2>
			</div>
			<table  class="table table-striped table-borderless table-vcenter">
            	<tbody id="end_info"></tbody>
            </table>
            
		</div>
		<!--<div id="app" class="block">
			<div class='block-title'>
				<h2><strong>Чат</strong> с исполнителем</h2>
			</div>
			
			<chat-box :id="{{ json_encode($id) }}" :user_id = "@if(Auth::check()) {{Auth::id()}} @else {{'0'}}@endif"></chat-box> 

			
		</div>-->
		<div class="block full">
			<div class="block-title">
			<h2><strong>Изменения</strong> заявки</h2>
			</div>
			<div class="timeline block-content-full">
				<ul class="timeline-list timeline-hover" id="log">
					
				</ul>
			</div>
		</div>
	</div>
	<div class="colum-md-6 col-lg-4">
		<div class="block">
			<div class="widget">
				<div class="widget-extra themed-background-success">
					<h4 class="widget-content-light"><strong>Статус заявки </strong></h4>
				</div>
				<div id="status" class="widget-extra-full"><span class="h2 text-success"><i class="fa fa-refresh fa-spin"></i></span></div>
				@if($work or $edit)
				<select id="statuschange" name="statuschange" class="select-select2 statuschange" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true" onchange="updateStatus(this.value)">

				</select>
				@endif
			</div>
		</div>
		<div class="block" > 
			<div class="block-title">
			    <h2><strong>Данные</strong> о исполнителе</h2>
			</div>
			<div id="worker"></div>
			@if($edit)
			<div>
				<div class="block-section">
					<table class="table table-borderless table-striped table-vcenter">
						<tr>
							<td><strong>Выберите исполнителя заявки</strong></td>
						</tr>
						<tr>
							<td>
								<select id="selectw" name="statuschange" class="select-select2 statuschange" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true" onchange="addWorker(this.value, 1)"></select>
							</td>
						</tr>
					</table>
				</div>
			</div>
			@endif
			<!--<div class="block-section">
				<div class="btn btn-lg btn-danger btn-block disabled">Исполнитель еще не назначен</div>
			</div>-->
		</div>
		<div class="block" > 
			<div class="block-title">
			    <h2><strong>Данные</strong> о соисполнителе</h2>
			</div>
			<div id="soworker"></div>
			@if($edit)
			<div>
				<div class="block-section">
					<table class="table table-borderless table-striped table-vcenter">
						<tr>
							<td><strong>Выберите соисполнителя заявки</strong></td>
						</tr>
						<tr>
							<td>
								<select id="selectsw" name="statuschange" class="select-select2 statuschange" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true" onchange="addWorker(this.value, 2)"></select>
							</td>
						</tr>
					</table>
				</div>
			</div>
			@endif
			
			<!--<div class="block-section">
				<div class="btn btn-lg btn-danger btn-block disabled">Исполнитель еще не назначен</div>
			</div>-->
		</div>
	</div>
</div>


@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		
        <script src="{{ asset('js/helpers/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('js/pages/request.js')}}"></script>
@stop