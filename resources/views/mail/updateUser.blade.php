<html>


<body>
	<div class="content">
		<h2>Обновлен статус вашей заявки №{{$component->id}}</h2>
		<div class="info">
			Текущий статус: {{$component->status}}<br>
			Вы можете отследить действия по вашей заявке при помощи кода: {{$component->key}}<br>
			По ссылке: <a href="https://support.miigaik.ru/request">https://support.miigaik.ru/request</a>
		</div>
	</div>
</body>
</html>