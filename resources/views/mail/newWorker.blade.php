<html>


<body>
	<div class="content">
		<h2>Назначен новый исполнитель вашей заявки №{{$component->id}}</h2>
		<div class="info">
			@if($component->user->fio)
				Исполнитель зявки: {{$component->user->fio}}<br>
			@endif
			@if($component->user->phone)
				Внутренний номер телефона исполнителя: {{$component->user->phone}}<br>
			@endif
			@if($component->user->email)
				Контактный email исполнителя: {{$component->user->email}}<br>
			@endif
			Вы можете отследить действия по вашей заявке при помощи кода: {{$component->key}}<br>
			По ссылке: <a href="https://support.miigaik.ru/request">https://support.miigaik.ru/request</a>
		</div>
	</div>
</body>
</html>