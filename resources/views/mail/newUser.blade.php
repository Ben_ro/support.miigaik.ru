<html>


<body>
	<div class="content">
		<h2>Новая заявка №{{$component->number}}</h2>
		<div class="info">
			Вы создали новую заявку на портале заявок МИИГАиК.<br>
			Указанная проблема: {{$component->problem}}<br>
			Комментарий: {{$component->comment}}<br>
			Вы можете отследить действия по вашей заявке при помощи кода: {{$component->key}}<br>
			По ссылке: <a href="https://support.miigaik.ru/request">https://support.miigaik.ru/request</a>
		</div>
	</div>
</body>
</html>