@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<div class="row">
	<div class="col-lg-6">
		<div class="block">
			<!-- All Orders Title -->
			<div class="block-title">
				<h2><strong>Добавить </strong> отдел</h2>
			</div>
			<!-- END All Orders Title -->
			<div class="form-group">
				@if(!empty($info['error']))
					{{$info['error']}}
				@endif
				@if(!empty($info['success']))
					{{$info['success']}}
				@endif
			</div>
			{!! Form::open(['url' => '/panel/department', 'class' => 'form-horizontal form-bordered', 'id' => 'clickable-wizard']) !!}
			<div class="form-group">
				{!! Form::label('name', 'Название структурного подразделения',['class' => 'col-md-5 control-label', 'autocomplete' => 'off', 'placeholder' => 'Фамилия Имя Отчество']) !!}
				<div class="col-md-5">
					{!! Form::text('name', '', ['class' => 'form-control', $add]) !!}
				</div>	
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Головное подразделение',['class' => 'col-md-5 control-label', 'autocomplete' => 'off', 'placeholder' => 'Фамилия Имя Отчество']) !!}
				<div class="col-md-5">
					<select id="select" name="select" class="select-select2" style="width: 100%;" data-placeholder="Choose one.." {{$add}}>
						<option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
						@foreach($department as $dep)
							<option value="{{$dep->id}}">{{$dep->name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<center>{!! Form::button('Добавить', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary', $add]) !!}</center>
			</div>
			{!! Form::close() !!}
			<!-- END All Orders Content -->
		</div>
	</div>
	<div class="col-lg-6">
		<div class="block">
			<!-- All Orders Title -->
			<div class="block-title">
				<h2><strong>Существующие </strong> отделы</h2>
			</div>
			<!-- END All Orders Title -->
			<div class="form-group">
				
				<div class="list-group" id="department">
					@foreach($department_e as $dep)
						<div id="dep-{{$dep->id}}">
							@role('admin')
							<a onclick="departmentClose({{$dep->id}})" class="close" id="del-{{$dep->id}}"><i class="fa fa-times"></i></a>
							@endrole
							<a href="/panel/department/{{$dep->id}}" class="list-group-item">
								<h5 class="list-group-item-heading">{{$dep->name}}</h5>
							</a>
						</div>
						@if(isset($parrent[$dep->id]))
							<div class="list-group">
								@foreach($parrent[$dep->id] as $otd)
								<div id="dep-{{$otd->id}}">
									@role('admin')
									<a onclick="departmentClose({{$otd->id}})" class="close"><i class="fa fa-times"></i></a>
									@endrole
									<a href="/panel/department/{{$otd->id}}" class="list-group-item">
										<h5 class="list-group-item-heading">{{$otd->name}}</h5>
									</a>
								</div>
								@endforeach
							</div>
						@endif
					@endforeach
					
				</div>
			</div>
			<!-- END All Orders Content -->
		</div>
	</div>
</div>
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		
		<script src="{{ asset('js/pages/adminRequests.js')}}"></script>
        <script>$(function(){ adminRequests.init(); });</script>
        
@stop