@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<div class="page-content">
	<div class="content-header content-header-media" style="height:100px">
		<div class="header-section">
			<h1>{{$dep->name}}</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-lg-7">
			<div class="block">
				<!-- Info Title -->
				<div class="block-title">
					<h2>Информация об отделе <strong>{{$dep->name}}</strong></h2>
				</div>
				<!-- END Info Title -->

				<!-- Info Content -->
				<table class="table table-borderless table-striped">
					<tbody>
						<tr>
							<td style="width: 20%;"><strong>Описание</strong></td>
							<td id="editinfo_{{$dep->id}}">{{$dep->info}}</td>
							<td style="width: 35px;">
								<div class="btn-group btn-group-xs" >
									<a href="javascript:infoEdit({{$dep->id}}, '{{$dep->info}}')" class="btn btn-default" id="editinfob_{{$dep->id}}"><i class="hi hi-pencil"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td><strong>Расположение</strong></td>
							<td id="editaddress_{{$dep->id}}">{{$dep->address}}</td>
							<td style="width: 35px;">
								<div class="btn-group btn-group-xs" >
									<a href="javascript:addressEdit({{$dep->id}}, '{{$dep->address}}')" class="btn btn-default" id="editaddressb_{{$dep->id}}"><i class="hi hi-pencil"></i></a>
								</div>
							</td>
						</tr>
						<tr>
							<td><strong>Начальник</strong></td>
							<td>
								@if(is_array($chief))
								@foreach($chief as $user)
								<a href="javascript:void(0)">{{$user['fio']}}</a>
								@endforeach
								@endif
							</td>
							<td></td>
						</tr>
						<tr>
							<td><strong>Заявок</strong></td>
							<td>
								<a href="javascript:void(0)" class="label label-info">{{count($requests[1])}}</a>
								<a href="javascript:void(0)" class="label label-warning">{{count($requests[2])}}</a>
								<a href="javascript:void(0)" class="label label-success">{{count($requests[3])}}</a>
							</td>
							<td></td>
						</tr>
						
						
						
					</tbody>
				</table>
				<!-- END Info Content -->
			</div>
			<div class="block">
				<!-- Friends Title -->
				<div class="block-title">
					<h2>Сотрудники отдела <strong>{{$dep->name}}</strong> <small>• <a href="javascript:void(0)">Все</a></small></h2>
				</div>
				<!-- END Friends Title -->

				<!-- Friends Content -->
				<div class="row text-center">
				@if(is_array($usersDepartment))
					@foreach($usersDepartment as $user)
					<div class="col-xs-4 col-sm-3 col-lg-1 block-section">
						<a href="javascript:void(0)">
							<img src="{{ asset('img/placeholders/avatars/avatar13.jpg')}}" alt="image" class="img-circle" data-toggle="tooltip" title="" data-original-title="{{$user['roleName']}}">
							<p>{{$user['fio']}}</p>
						</a>
					</div>
					@endforeach
				@endif
					

				</div>
				<!-- END Friends Content -->
			</div>
		</div>
		<div class="col-md-6 col-lg-5">
			<div class="block">
				<!-- Info Title -->
				<div class="block-title">
					<h2>Типы исполняемых заявок<strong></strong></h2>
				</div>
				<!-- END Info Title -->
				<div class="form-group">
					<table class="table table-striped table-vcenter" id="type-list"><tbody></tbody></table>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-lg-4"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
	<script src="{{ asset('js/pages/department.js')}}"></script>
     <script>$(function(){ typeGet(); });</script>
        
@stop