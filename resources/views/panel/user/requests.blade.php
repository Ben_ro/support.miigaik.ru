@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<div class="block full">
	<div class="block-title">
		<div class="block-options pull-right">
			<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
		</div>
		<h2><strong>Все </strong> Заявки</h2>
	</div>
	<div class="row">
		<div class="col-sm-6 col-lg-2">
			<a href="javascript:newRequet()" class="widget widget-hover-effect2">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light"><strong>Новая</strong> </h4>
				</div>
			</a>
        </div>
		<div class="col-sm-6 col-lg-2">
			<a href="javascript:workRequet()" class="widget widget-hover-effect2">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light"><strong>В работе</strong> </h4>
				</div>
			</a>
        </div>
		<div class="col-sm-6 col-lg-2">
			<a href="javascript:closeRequet()" class="widget widget-hover-effect2">
				<div class="widget-extra themed-background-dark">
					<h4 class="widget-content-light"><strong>Закрыта</strong> </h4>
				</div>
			</a>
        </div>
	</div>
	<div id="app">
		<table id="newRequets" class="table table-bordered table-striped table-vcenter" style="width: 100% !important"> 
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<th class="">Проблема</th>
					<th>Время создания</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<table id="workRequets" class="table table-bordered table-striped table-vcenter" style="width: 100% !important">
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<th class="">Проблема</th>
					<th>Время создания</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<table id="closeRequets" class="table table-bordered table-striped table-vcenter" style="width: 100% !important">
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<th class="">Проблема</th>
					<th>Время создания</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		<script src="{{ asset('js/pages/userRequests.js')}}"></script>
        <script>$(function(){ newRequets.init(); });</script>
		 <script>$(function(){ workRequets.init(); });</script>
		  <script>$(function(){ closeRequets.init(); });</script>
@stop
