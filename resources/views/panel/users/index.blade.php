@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<!-- Clickable Wizard Block -->
<div class="block">
	<div class="block-title">
		<h2><strong>Панель</strong> сотрудников</h2>
	</div>
<div class="row">
	<div class="col-sm-1 col-lg-2"></div>
    <div class="col-sm-2 col-lg-3">
        <!-- Widget -->
        <a href="/panel/users/add" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                    
                    <i class="hi hi-plus"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>Добавить сотрудника</strong><br>
                    <small></small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
	<div class="col-sm-1 col-lg-2"></div>
	<div class="col-sm-2 col-lg-3">
        <!-- Widget -->
        <a href="/panel/users/role" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                   <i class="fa fa-file-text"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>Настройка доступа</strong>
                    <small>Настройка прав доступа к функциям сайта</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
	<div class="col-sm-1 col-lg-2"></div>
    
</div>							
</div>
@yield('user_content')
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')
		<script src="{{ asset('js/pages/formsWizard.js')}}"></script>
        <script src="{{ asset('js/helpers/ckeditor/ckeditor.js')}}"></script>
		@yield('user_js')
@stop
