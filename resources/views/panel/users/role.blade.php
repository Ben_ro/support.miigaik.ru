@extends('panel.users.index')
@section('title', 'Заголовок страницы')
@section('user_content')
<div class="row">
	<div class="col-lg-12">
		<div class="block">
			<!-- All Orders Title -->
			<div class="block-title">
				<h2><strong>Добавить </strong> права</h2>
			</div>
			<!-- END All Orders Title -->
			<div class="form-group">
				@if(!empty($info['error']))
					{{$info['error']}}
				@endif
				@if(!empty($info['success']))
					{{$info['success']}}
				@endif
			</div>
			{!! Form::open(['url' => 'panel/users/role/infoAdd', 'class' => 'form-horizontal form-bordered', 'id' => 'form-validation']) !!}
			<div class="form-group">
			<fieldset>
				<div class="wrap">
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-clickable-firstname">Выберите тип</label>
						<div class="col-md-5">
						   <select id="type" name="type" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."  aria-required="true">
								<option></option>
								<option value="roleAdd">Добавить Роль</option>
								<option value="premissionAdd">Добавить Правила</option>
								<option value="roleRemove">Удалить роль</option>
								<option value="permissionRemove">Удалить правило</option>
						   </select>
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('name', 'Краткое название на Английском',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
						<div class="col-md-5">
							{!! Form::text('name', $request->name, ['class' => 'form-control']) !!}	
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('display_name', 'Название которе убдет показываться',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
						<div class="col-md-5">
							{!! Form::text('display_name', $request->display_name, ['class' => 'form-control']) !!}	
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('description', 'Описание',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
						<div class="col-md-5">
							{!! Form::text('description', $request->description, ['class' => 'form-control']) !!}	
						</div>
					</div>
				</div>
			</fieldset>
			</div>
			<div class="form-group">
				<div class="form-group form-actions">
					<div class="col-md-8 col-md-offset-4">
						{!! Form::button('<i class="fa fa-arrow-right"></i>Добавить', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']) !!}
						{!! Form::button('<i class="fa fa-repeat"></i> Сбросить', ['type' => 'reset', 'class' => 'btn btn-sm btn-warning']) !!}
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="block">
			<!-- All Orders Title -->
			<div class="block-title">
				<h2><strong>Настройка </strong> ролей</h2>
			</div>

			{!! Form::open(['url' => 'panel/users/role/permission', 'class' => 'form-horizontal form-bordered', 'id' => 'form-validation']) !!}
			<div class="form-group">
			<fieldset>
				<table id="general-table" class="table table-striped table-vcenter">
					<thead>
						<tr>
							<th class="text-center">Правило\Роль</th>
							@foreach($roles as $role)
							<th class="text-center">{{$role['display_name']}}</td>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach($permissions as $permission)
						<tr>
							<td  class="text-center">{{$permission->display_name}} ( <b>{{$permission->name}}</b> )</td>
							@foreach($roles as $role)
							<td  class="text-center"><input type="checkbox" name="rol[{{$role['id']}}][{{$permissionRoleList[$role['id']][$permission->id][1]}}]" @if($permissionRoleList[$role['id']][$permission->id][0])checked="checked"@endif /></td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
                </table>
			</fieldset>
		</div>
		<div class="form-group">
				<div class="form-group form-actions">
					<div class="col-md-7 col-md-offset-5">
						{!! Form::button('<i class="fa fa-arrow-right"></i>Добавить', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']) !!}
						{!! Form::button('<i class="fa fa-repeat"></i> Сбросить', ['type' => 'reset', 'class' => 'btn btn-sm btn-warning']) !!}
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
@stop
@section('user_js')
		
		<!--<script src="{{ asset('js/pages/formsInv.js')}}"></script>
       <script>$(function(){ lol(); });</script>-->
        
@stop