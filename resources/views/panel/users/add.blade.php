@extends('panel.users.index')
@section('title', 'Заголовок страницы')
@section('user_content')
<div class="block full">
                            <!-- All Orders Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                </div>
                                <h2><strong>Регистрация </strong> сотрудника</h2>
                            </div>
                            
                            <!-- END All Orders Title -->
                            <div class="form-group">
                            	@if(!empty($info))
                            		{{$info}}
	                            @endif
	                            
                            </div>
							{!! Form::open(['url' => 'panel/users/add', 'class' => 'form-horizontal form-bordered', 'id' => 'form-validation']) !!}
								<div class="form-group">
								<fieldset>
									<legend><i class="fa fa-angle-right"></i> Регистрационные данные</legend>
									<div class="wrap">
										<div class="form-group">
											{!! Form::label('name', 'Логин',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::text('name', $request->name, ['class' => 'form-control']) !!}	
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('email', 'Почта',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::text('email', $request->email, ['class' => 'form-control']) !!}	
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('name', 'Пароль',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::password('password', ['class' => 'form-control']) !!}	
											</div>
										</div>
									</div>
								</fieldset>
								</div>
								<div class="form-group">
								<fieldset>
									<legend><i class="fa fa-angle-right"></i> Информация о пользователе</legend>
									<div class="wrap">
										<div class="form-group">
											{!! Form::label('fio', 'ФИО',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::text('fio', $request->fio, ['class' => 'form-control']) !!}	
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('date', 'Дата рождения',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::text('date', $request->date, ['class' => 'form-control']) !!}	
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('phone', 'Внутренний номер телефона',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												{!! Form::text('phone', $request->phone, ['class' => 'form-control']) !!}	
											</div>
										</div>
									</div>
								</fieldset>
								</div>
								<div class="form-group">
								<fieldset>
									<legend><i class="fa fa-angle-right"></i> Работник</legend>
									<div class="wrap">
									<div class="form-group">
										<label class="col-md-4 control-label" for="example-clickable-firstname">Выберите отдел</label>
	                                    <div class="col-md-5">
	                                       <select id="department" name="department" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."  aria-required="true">
												<option></option>
												@foreach($department as $dep)
													<option value="{{$dep->id}}">{{$dep->name}}</option>
                                                @endforeach
										   </select>
		                                </div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label" for="example-clickable-firstname">Выберите должность</label>
	                                    <div class="col-md-5">
	                                       <select id="group" name="group" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."  aria-required="true">
												<option></option>
												@foreach($roles as $role)
													<option value="{{$role->id}}">{{$role->name}}</option>
												@endforeach
										   </select>
		                                </div>
									</div>
									</div>
								</fieldset>
								</div>
								<div class="form-group">
                                    <div class="form-group form-actions">
                                        <div class="col-md-8 col-md-offset-4">
                                            {!! Form::button('<i class="fa fa-arrow-right"></i>Добавить', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']) !!}
                                            {!! Form::button('<i class="fa fa-repeat"></i> Сбросить', ['type' => 'reset', 'class' => 'btn btn-sm btn-warning']) !!}
                                        </div>
                                    </div>
   								 </div>
							{!! Form::close() !!}
                            <!-- END All Orders Content -->
                            </div>
                            
                       
@stop
@section('user_js')
		
		<!--<script src="{{ asset('js/pages/formsInv.js')}}"></script>
       <script>$(function(){ lol(); });</script>-->
        
@stop