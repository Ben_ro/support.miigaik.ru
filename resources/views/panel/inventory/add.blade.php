@extends('panel.inventory.index')
@section('inv_content')
<div class="block full">
                            <!-- All Orders Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                </div>
                                <h2><strong>Добавить </strong> объект инвентаризации</h2>
                            </div>
                            
                            <!-- END All Orders Title -->
                            <div class="form-group">
                            	@if(!empty($info))
                            		{{$info}}
	                            @endif
	                            
                            </div>
							{!! Form::open(['url' => 'panel/inventory/add', 'class' => 'form-horizontal form-bordered', 'id' => 'form-validation']) !!}
								<div class="form-group">
									<fieldset>
                                        <legend><i class="fa fa-angle-right"></i> Расположение</legend>
                                    	<div class="form-group">
	                                        <label class="col-md-1 control-label" for="example-clickable-firstname">Выберите корпус</label>
	                                        	<div class="col-md-3">
	                                        		<select id="val_build" name="val_build" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."  onChange="level_get(this.value)"  aria-required="true"></select>
		                                        </div>
		                                        <label class="col-md-1 control-label" for="example-clickable-firstname">Выберите этаж</label>
	                                    		<div class="col-md-3">
	                                        		<select id="val_level" name="val_level" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."  onChange="room_get(this.value)"  aria-required="true"></select>
		                                		 </div>
		                                		 <label class="col-md-1 control-label" for="example-clickable-firstname">Выберите кабинет</label>
		                                     	<div class="col-md-3">
		                                        		<select id="val_room" name="val_room" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true"></select>
			                                    </div>
                                    	</div>
                                    </fieldset>
                                    <fieldset>
                                        <legend><i class="fa fa-angle-right"></i> Информация</legend>
                                        <div class="form-group">
	                                        <label class="col-md-4 control-label" for="example-clickable-firstname">Выберите тип техники</label>
	                                        <div class="col-md-5">
	                                        	<select id="val_type" name="val_type" class="select-select2" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true">
														<option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                        @foreach($types as $type)
															<option value="{{$type->id}}">{{$type->name}}</option>
														@endforeach
                                                        
	                                        	</select>
		                                    </div>
                                    	</div>
                                    	<div class="form-group">
	                                        {!! Form::label('val_inv', 'Инвентарный номер устройства',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
	                                        <div class="col-md-5">
	                                        	{!! Form::text('val_inv', '', ['class' => 'form-control']) !!}	
	                                        </div>
                                    	</div>
										<div class="form-group">
	                                        {!! Form::label('val_count', 'Количество',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
	                                        <div class="col-md-5">
	                                        	{!! Form::text('val_count', '', ['class' => 'form-control']) !!}	
	                                        </div>
                                    	</div>
                                    	<div class="form-group">
											{!! Form::label('name', 'Описание устройства',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
	                                        <div class="col-md-5">
	                                        	{!! Form::textarea('val_info', '', ['class' => 'form-control']) !!}	
	                                        </div>
                                    	</div>
                                    	<div class="form-group">
											{!! Form::label('val_inv', 'Стутус',['class' => 'col-md-4 control-label', 'autocomplete' => 'off']) !!}
											<div class="col-md-5">
												<select id="val_status" name="val_status" class="form-control" size="1">
													<option value="0">Активно</option>
													<option value="1">На ремонте</option>
													<option value="2">Списан</option>
												</select>
											</div>
                                    	</div>
                                    </fieldset>   
                                    <div class="form-group form-actions">
                                        <div class="col-md-8 col-md-offset-4">
                                            {!! Form::button('<i class="fa fa-arrow-right"></i>Добавить', ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']) !!}
                                            {!! Form::button('<i class="fa fa-repeat"></i> Сбросить', ['type' => 'reset', 'class' => 'btn btn-sm btn-warning']) !!}
                                        </div>
                                    </div>
   								 </div>
							{!! Form::close() !!}
                            <!-- END All Orders Content -->
                            </div>
                            
                       
@stop
@section('inv_js')
		<script src="{{ asset('js/pages/formsWizard.js')}}"></script>
        <script src="{{ asset('js/helpers/ckeditor/ckeditor.js')}}"></script>
		<script src="{{ asset('js/pages/formsInv.js')}}"></script>
       <script>$(function(){ lol(); });</script>
        
@stop