@extends('index')
@section('title', 'Заголовок страницы')
@section('page-content')
<!-- Clickable Wizard Block -->
<div class="block">
	<div class="block-title">
		<h2><strong>Панель</strong> Инвентаризации</h2>
	</div>
<div class="row">
    @permission('inv-add')
	<div class="col-sm-6 col-lg-4">
        <!-- Widget -->
        <a href="/panel/inventory/add" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                    
                    <i class="hi hi-plus"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>Добавить</strong><br>
                    <small>Новое оборудование</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
	@endpermission
	@permission('inv-maps')
    <div class="col-sm-6 col-lg-4">
        <!-- Widget -->
        <a href="/panel/inventory/maps/feut" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                    
                    <i class="fa fa-map-o"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                     <strong>Интерактивная карта</strong><br>
                    <small>Со списком оборудования в каждой аудитории</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
	@endpermission
	@permission('inv-spisok')
    <div class="col-sm-6 col-lg-4">
        <!-- Widget -->
        <a href="/panel/inventory/list" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                   <i class="fa fa-file-text"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    <strong>Список</strong>
                    <small>Имущества организации</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
	@endpermission
</div>							
</div>
@yield('inv_content')
@stop
@section('footer')
	<div class="pull-right">
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="" target="_blank">Заявки</a>
	</div>
@stop
@section('js')

		@yield('inv_js')
@stop
