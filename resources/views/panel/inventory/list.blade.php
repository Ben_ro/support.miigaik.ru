@extends('panel.inventory.index')
@section('title', 'Заголовок страницы')
@section('inv_content')
<!-- Clickable Wizard Block -->

<div class="block">
	<div class="block-title">
		<h2><strong>Панель</strong> Инвентаризации</h2>
	</div>
	<div class="form-group">
		<fieldset>
		<legend><i class="fa fa-angle-right"></i> Расположение</legend>
		<div class="form-group">
			<label class="col-md-1 control-label" for="example-clickable-firstname">Поиск по типу</label>
			<div class="col-md-3">
				<select id="col0_filter" name="col0_filter" class="select-select2 col0_filter" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true">
					<option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
					<option value="Системный блок">Системный блок</option>
					<option value="Монитор">Монитор</option>
					<option value="Принтер">Принтер</option>
					<option value="Картридж">Картридж</option>
				</select>
			</div>
			<label class="col-md-1 control-label" for="example-clickable-firstname">Поиск по корпусу</label>
			<div class="col-md-3">
				<select id="col0_filter" name="col0_filter" class="select-select2 col0_filter" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true">
					<option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
					<option value="Системный блок">Системный блок</option>
					<option value="Монитор">Монитор</option>
					<option value="Принтер">Принтер</option>
					<option value="Картридж">Картридж</option>
				</select>
			</div>
			<label class="col-md-1 control-label" for="example-clickable-firstname">Поиск по кабинету</label>
			<div class="col-md-3">
				<select id="col0_filter" name="col0_filter" class="select-select2 col0_filter" style="width: 100%;" data-placeholder="Выберите.."   aria-required="true">
					<option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
					<option value="Системный блок">Системный блок</option>
					<option value="Монитор">Монитор</option>
					<option value="Принтер">Принтер</option>
					<option value="Картридж">Картридж</option>
				</select>
			</div>
		</div>
		</fieldset>
	</div>
	
	<div class="form-group">
	<fieldset>
	<!--<table cellpadding="3" cellspacing="0" border="0" style="width: 67%; margin: 0 auto 2em auto;">
        <thead>
            <tr>
                <th>Target</th>
                <th>Search text</th>
                <th>Treat as regex</th>
                <th>Use smart search</th>
            </tr>
        </thead>
        <tbody>
            <tr id="filter_global">
                <td>Global search</td>
                <td align="center"><input type="text" class="global_filter" id="global_filter"></td>
                <td align="center"><input type="checkbox" class="global_filter" id="global_regex"></td>
                <td align="center"><input type="checkbox" class="global_filter" id="global_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col1" data-column="0">
                <td>Column - Name</td>
                <td align="center"><input type="text" class="column_filter" id="col0_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col0_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col0_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col2" data-column="1">
                <td>Column - Position</td>
                <td align="center"><input type="text" class="column_filter" id="col1_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col1_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col1_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col3" data-column="2">
                <td>Column - Office</td>
                <td align="center"><input type="text" class="column_filter" id="col2_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col2_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col2_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col4" data-column="3">
                <td>Column - Age</td>
                <td align="center"><input type="text" class="column_filter" id="col3_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col3_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col3_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col5" data-column="4">
                <td>Column - Start date</td>
                <td align="center"><input type="text" class="column_filter" id="col4_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col4_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col4_smart" checked="checked"></td>
            </tr>
            <tr id="filter_col6" data-column="5">
                <td>Column - Salary</td>
                <td align="center"><input type="text" class="column_filter" id="col5_filter"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col5_regex"></td>
                <td align="center"><input type="checkbox" class="column_filter" id="col5_smart" checked="checked"></td>
            </tr>
        </tbody>
    </table>-->
	<table id="inventoryList" class="table table-bordered table-striped table-vcenter" style="width: 100%;">
		<thead>
			<tr>
			    <th class="text-center" style="width: 100px;">Инв. номер</th>
			    <th>Тип</th>
			    <th class="hidden-xs">Корпус</th>
			    <th class="text-center hidden-xs">Кабинет</th>
			    <th class="hidden-xs">Описание</th>
			    <th>Статус</th>
				<th class="text-center">Кол-во</th>
			</tr>
		</thead> 
    	<tbody></tbody>
	</table>
	</fieldset>
	</div>	
	
</div>
@stop
@section('inv_js')
		<script src="/js/pages/inventoryList.js"></script>
        <script>$(function(){ InventoryList.init(); });</script>
@stop