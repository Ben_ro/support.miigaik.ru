@extends('panel.inventory.index')
@section('inv_content')
<div class="block full">
                            <!-- All Orders Title -->
                            <div class="block-title">
                                <div class="block-options pull-right">
                                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                </div>
                                <h2><strong>Карта </strong> инвентаризации</h2>
                            </div>
                            
                            <div id="mapplic"></div>
							<div id="info"></div>
                            
                       
@stop
@section('inv_js')
		<!--<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/map.css')}}">-->
		<link rel="stylesheet" type="text/css" href="{{ asset('js/mapplic/mapplic.css')}}">
		<script type="text/javascript" src="{{ asset('js/mapplic/js/hammer.min.test.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/mapplic/js/jquery.easing.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/mapplic/js/jquery.mousewheel.test.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/mapplic/js/smoothscroll.js')}}"></script>
		<script type="text/javascript" src="{{ asset('js/mapplic/mapplic.js')}}"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var mapplic = $('#mapplic').mapplic({
					source: '/panel/inventory/mapjson/{{$id}}',
					sidebar: false,
					minimap: false,
					markers: false,
					fullscreen: true,
					fillcolor: false,
					hovertip: true,
					developer: true,
					thumbholder: true,
					height: 450,
					maxscale: 3
				});
				
			
		

				/* Landing Page */
				$('.usage').click(function(e) {
					e.preventDefault();
					$('.editor-window').slideToggle(200);
				});

				$('.editor-window .window-mockup').click(function() {
					$('.editor-window').slideUp(200);
				});
			});
		</script>
        
@stop