<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>HASHINFO.RU - @yield('title')</title>
        <meta name="description" content="">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
		<meta name="csrf-token" content="{{ Session::token() }}"> 
        <link rel="shortcut icon" href="/img/favicon.png">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
      	<link rel="stylesheet" href="{{ asset('css/plugins.css')}}">
        <link rel="stylesheet" href="{{ asset('css/main.css')}}">
        <link rel="stylesheet" href="{{ asset('css/themes.css')}}">
        <link rel="stylesheet" href="{{ asset('css/themes/spring.css')}}">
          <!-- Scripts -->
       <!-- <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>-->
        
       
    </head>
    <body>
    
	    <header class="navbar navbar-default">
	    	<div class="navbar-header">
                            <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
                            <ul class="nav navbar-nav-custom  visible-xs">
                                <li>
                                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#horizontal-menu-collapse">Меню</a>
                                </li>
                                
                            </ul>
                           @guest
                           		
                           @else
                             <ul class="nav navbar-nav-custom pull-right visible-xs">
                            <!-- Alternative Sidebar Toggle Button -->
                           <li>
                                <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                                <a href="/panel/user/requests" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');this.blur();">
                                    <i class="fa fa-television"></i>
                                    <span class="label label-primary label-indicator animation-floating">{{$userRequests}}</span>
                                </a>
                            </li>
                            <!-- END Alternative Sidebar Toggle Button -->
							
                            <!-- User Dropdown -->
                            <li class="dropdown ">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i> 
                                </a>
                               <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="dropdown-header text-center">Account</li>
                                    <li>
                                        <!--<a href="">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Профиль
                                        </a>
                                        <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                       <!-- <a href="" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Настройки
                                        </a>-->
                                        
                                    	<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	                                         <i class="fa fa-cog fa-fw pull-right"></i>{{ __('Выход') }}
	                                    </a>

	                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>
                                        
                                    </li>
                                    
                                </ul>
                            </li>

                        </ul> 
                             <!-- Right Header Navigation -->
                       @endguest
                	</div>
			<!-- Horizontal Menu + Search -->
			<div id="horizontal-menu-collapse" class="collapse navbar-collapse">
		    	<ul class="nav navbar-nav">
		        	<li>
		            	<a href="/">Главная</a>
		        	</li>
		        	<li>
		            	<a href="/requests">Все заявки</a>
		        	</li>
		        	<li>
		            	<a href="/request">Узнать статус заявки</a>
		        	</li>
		        	
		        	@guest
                         <li>
		            	<a href="/login">Вход</a>
		        	</li>  		
                    @else
			        <li class="dropdown">
			            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Панель Управления  <i class="fa fa-angle-down"></i></a>
			            <ul class="dropdown-menu">
			                @ability('', 'inv-add,inv-spisok,inv-maps')<li><a href="/panel/inventory">Инвентаризация <i class="fa fa-asterisk fa-fw"></i></a></li>@endability
			                <li><a href="/panel/department">Управление отделами <i class="fa fa-lock fa-fw"></i></a></li>
			                <li><a href="/panel/requests">Управление заявками <i class="fa fa-magnet fa-fw"></i></a></li>
			                <li><a href="/panel/users">Сотрудники <i class="fa fa-user fa-fw"></i></a></li>
			                <!--<li class="divider"></li>
			                <li class="dropdown-submenu">
			                    <a href="javascript:void(0)" tabindex="-1"><i class="fa fa-chevron-right fa-fw pull-right"></i> More Settings</a>
			                    <ul class="dropdown-menu">
									<li><a href="javascript:void(0)" tabindex="-1">Second level</a></li>
									<li><a href="javascript:void(0)">Second level</a></li>
									<li><a href="javascript:void(0)">Second level</a></li>
									<li class="divider"></li>
									<li class="dropdown-submenu">
									    <a href="javascript:void(0)" tabindex="-1"><i class="fa fa-chevron-right fa-fw pull-right"></i> More Settings</a>
									    <ul class="dropdown-menu">
									        <li><a href="javascript:void(0)">Third level</a></li>
									        <li><a href="javascript:void(0)">Third level</a></li>
									        <li><a href="javascript:void(0)">Third level</a></li>
									    </ul>
									</li>
		                    	</ul>
		                	</li>-->
		            	</ul>
		        	</li>
		        	@endguest
			        <!--<li class="dropdown">
			            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Contact <i class="fa fa-angle-down"></i></a>
			            <ul class="dropdown-menu">
			                <li><a href="javascript:void(0)"><i class="fa fa-envelope-o fa-fw pull-right"></i> By Email</a></li>
			                <li><a href="javascript:void(0)"><i class="fa fa-phone fa-fw pull-right"></i> By Phone</a></li>
			            </ul>
			        </li>-->
			    </ul>
			    @guest  
			    		
                @else
			    <ul class="nav navbar-nav-custom pull-right hidden-xs">
                            <!-- Alternative Sidebar Toggle Button -->
                           <li>
                                <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                                <a href="/panel/user/requests">
                                    <i class="fa fa-television"></i>
                                    <span class="label label-primary label-indicator animation-floating">{{$userRequests}}</span>
                                </a>
                            </li>
                            <!-- END Alternative Sidebar Toggle Button -->

                            <!-- User Dropdown -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="dropdown-header text-center">Account</li>
                                    <li>
                                        <!--<a href="">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Профиль
                                        </a>
                                        <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                        <!--<a href="" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Настройки
                                        </a>-->
                                        
                                    	<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	                                         <i class="fa fa-cog fa-fw pull-right"></i>{{ __('Выход') }}
	                                    </a>

	                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>
                                    </li>
                                    
                                </ul>
                            </li>

                        </ul> 
                        @endguest
			</div>
		</header>
	    <div id="page-content">
	    	
			@yield('page-content')
			@yield('modal')
		</div>
		<footer class="clearfix">
			@yield('footer')
		</footer>
		
		<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
		<script src="{{ asset('js/vendor/jquery.min.js')}}"></script>
		<script src="{{ asset('js/vendor/bootstrap.min.js')}}"></script>
		<script src="{{ asset('js/plugins.js')}}"></script>
		<script src="{{ asset('js/app1.js')}}"></script>
		
		@yield('js')
	</body>
</html>